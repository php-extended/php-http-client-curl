<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\CurlBodyHandler;
use PhpExtended\HttpClient\CurlOptions;
use PHPUnit\Framework\TestCase;

/**
 * CurlBodyHandlerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\CurlBodyHandler
 *
 * @internal
 *
 * @small
 */
class CurlBodyHandlerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlBodyHandler
	 */
	protected CurlBodyHandler $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CurlBodyHandler(new CurlOptions());
	}
	
}
