<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\CurlHeaderHandler;
use PhpExtended\HttpClient\CurlOptions;
use PHPUnit\Framework\TestCase;

/**
 * CurlHeaderHandlerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\CurlHeaderHandler
 *
 * @internal
 *
 * @small
 */
class CurlHeaderHandlerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlHeaderHandler
	 */
	protected CurlHeaderHandler $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CurlHeaderHandler(new CurlOptions());
	}
	
}
