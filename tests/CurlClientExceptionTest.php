<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\CurlClientException;
use PHPUnit\Framework\TestCase;

/**
 * CurlClientExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\CurlClientException
 *
 * @internal
 *
 * @small
 */
class CurlClientExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlClientException
	 */
	protected CurlClientException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(CurlClientException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CurlClientException();
	}
	
}
