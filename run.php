<?php declare(strict_types=1);

/**
 * This script is to test the curl methods. (Needs require from composer).
 * 
 * Usage : php test.php <url> <option=value> <...>
 * 
 * @author Anastaszor
 */

use PhpExtended\HttpClient\CurlClient;
use PhpExtended\HttpClient\CurlOptions;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Uri;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the url to fetch.');
}

$autoload = __DIR__.'/vendor/autoload.php';
if(!is_file($autoload))
{
	throw new RuntimeException('Composer must be runned first.');
}
require $autoload;

$request = new Request();
$request = $request->withMethod('GET');
$request = $request->withUri(Uri::parseFromString($argv[1]));

$options = new CurlOptions();

$otherArgs = array_splice($argv, 3);
foreach($otherArgs as $otherArg)
{
	$parts = explode('=', $otherArg, 2);
	if(count($parts) === 1)
	{
		switch($parts)
		{
			
			default:
				throw new RuntimeException('Unrecognized noarg option '.$parts);
		}
	}
	else	// count($parts) === 2
	{
		switch($parts)
		{
			
			default:
				throw new RuntimeException('Unrecognized arg option '.$parts);
		}
	}
}

$curlProcessor = new CurlClient($options);
$response = $curlProcessor->sendRequest($request);
echo $response->getBody()->__toString();

