<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Curl\CurlHeaderFunctionInterface;
use PhpExtended\Curl\CurlInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * CurlHeaderHandler class file.
 * 
 * This class represents the curl http handler to receive the parsed headers
 * from the curl library.
 * 
 * @author Anastaszor
 */
class CurlHeaderHandler implements CurlHeaderFunctionInterface
{
	
	/**
	 * The options for the configuration.
	 * 
	 * @var CurlOptions
	 */
	protected CurlOptions $_options;
	
	/**
	 * The full header data from the curl handle.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new CurlHeaderHandler with the given curl options.
	 * 
	 * @param CurlOptions $options
	 */
	public function __construct(CurlOptions $options)
	{
		$this->_options = $options;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHeaderFunctionInterface::write()
	 */
	public function write(CurlInterface $curl, string $headerStr) : int
	{
		$header = $this->_options->getFileHeader();
		if(null !== $header)
		{
			return (int) \fwrite($header, $headerStr);
		}
		
		$this->_data[] = $headerStr;
		
		return (int) \mb_strlen($headerStr, '8bit');
	}
	
	/**
	 * Applies the header handler to the response, effectively giving headers
	 * to the response. This returns a response with headers.
	 * 
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 */
	public function applyTo(ResponseInterface $response) : ResponseInterface
	{
		$header = $this->_options->getFileHeader();
		if(null !== $header)
		{
			$smd = \array_merge(['uri' => ''], (array) \stream_get_meta_data($header));
			$uri = $smd['uri'];
			if(\is_file($uri))
			{
				$contents = @\file_get_contents($uri);
				if(false !== $contents)
				{
					$this->_data = \explode("\r\n", $contents);
				}
			}
		}
		
		foreach($this->_data as $data)
		{
			if(false !== \mb_strpos($data, ':'))
			{
				$parts = \explode(':', $data, 2);
				/** @phpstan-ignore-next-line */
				if(isset($parts[0], $parts[1]))
				{
					try
					{
						$response = $response->withAddedHeader($parts[0], $parts[1]);
					}
					catch(InvalidArgumentException $exc)
					{
						// ignore
					}
				}
			}
		}
		
		return $response;
	}
	
}
