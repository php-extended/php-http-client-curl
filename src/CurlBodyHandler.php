<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Curl\CurlInterface;
use PhpExtended\Curl\CurlWriteFunctionInterface;
use PhpExtended\HttpMessage\FileStream;
use PhpExtended\HttpMessage\StringStream;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

/**
 * CurlBodyHandler class file.
 * 
 * This class represents the curl http handler to receive the parsed body
 * contents from the curl library.
 * 
 * @author Anastaszor
 */
class CurlBodyHandler implements CurlWriteFunctionInterface
{
	
	/**
	 * The options for the configuration.
	 * 
	 * @var CurlOptions
	 */
	protected CurlOptions $_options;
	
	/**
	 * The full body data from the curl handle.
	 * 
	 * @var string
	 */
	protected string $_data = '';
	
	/**
	 * Builds a new HttpCurlHeaderHandler with the given curl options.
	 * 
	 * @param CurlOptions $options
	 */
	public function __construct(CurlOptions $options)
	{
		$this->_options = $options;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlWriteFunctionInterface::write()
	 */
	public function write(CurlInterface $curl, string $data) : int
	{
		$stdout = $this->_options->getFileStdout();
		if(null !== $stdout)
		{
			return (int) \fwrite($stdout, $data);
		}
		
		$this->_data .= $data;
		
		return (int) \mb_strlen($data, '8bit');
	}
	
	/**
	 * Applies the body handler to the response, effectively giving a body
	 * to the response. This returns a response with body.
	 * 
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 */
	public function applyTo(ResponseInterface $response) : ResponseInterface
	{
		$stdout = $this->_options->getFileStdout();
		if(null === $stdout)
		{
			try
			{
				return $response->withBody(new StringStream($this->_data));
			}
			catch(InvalidArgumentException $e)
			{
				return $response;
			}
		}
		
		try
		{
			return $response->withBody(new FileStream($stdout));
		}
		catch(InvalidArgumentException|RuntimeException $e)
		{
			return $response;
		}
	}
	
}
