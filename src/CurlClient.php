<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Exception;
use InvalidArgumentException;
use PhpExtended\Certificate\CertificateProviderInterface;
use PhpExtended\Curl\Curl;
use PhpExtended\HttpMessage\Response;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * CurlClient class file.
 * 
 * This class is a simple implementation of the ProcessorInterface using curl.
 * 
 * @author Anastaszor
 */
class CurlClient implements ClientInterface, Stringable
{
	
	/**
	 * The certificate provider.
	 * 
	 * @var CertificateProviderInterface
	 */
	protected CertificateProviderInterface $_provider;
	
	/**
	 * Additional options to add to the curl handle.
	 * 
	 * @var ?CurlOptions
	 */
	protected ?CurlOptions $_options = null;
	
	/**
	 * Builds a new CurlClient with the given configuration.
	 * 
	 * @param CertificateProviderInterface $provider
	 * @param ?CurlOptions $options
	 */
	public function __construct(CertificateProviderInterface $provider, ?CurlOptions $options = null)
	{
		$this->_provider = $provider;
		$this->_options = $options;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$factory = new CurlOptionsFactory($this->_provider);
		$options = $factory->createFromRequest($request);
		if(null !== $this->_options)
		{
			$options = $this->_options->mergeWith($options);
		}
		
		$cafilepath = $this->_provider->getCertificateFilePath();
		if(null === $options->getCaInfo())
		{
			$options->setCaInfo($cafilepath);
		}
		if(null === $options->getCaPath())
		{
			$options->setCaPath($cafilepath);
		}
		
		try
		{
			$curl = new Curl();
		}
		catch(Exception $exc)
		{
			throw new CurlClientException('Failed to initialize curl', -1, $exc);
		}
		
		$curl->setUrl($request->getUri());
		$errors = $options->applyTo($curl);
		if(!empty($errors))
		{
			$message = 'Failed to apply options to curl : {errlist}';
			$context = ['{errlist}' => \implode('', \array_map(function(Exception $exc)
			{
				return \strtr('Curl Error {k} : {msg}', ['{k}' => $exc->getCode(), '{msg}' => $exc->getMessage()]);
			}, $errors))];
			
			throw new CurlClientException(\strtr($message, $context));
		}
		
		$response = new Response();
		
		try
		{
			/** @var ResponseInterface $response */
			$response = $response->withHeader('X-Target-Uri', $request->getUri()->__toString());
		}
		catch(InvalidArgumentException $e)
		{
			// nothing to do
		}
		
		$headerHandler = new CurlHeaderHandler($options);
		$bodyHandler = new CurlBodyHandler($options);
		$curl->setHeaderFunction($headerHandler);
		$curl->setWriteFunction($bodyHandler);
		
		if(!$curl->execute())
		{
			$message = 'Impossible to load url "{url}" (curl error : {err})';
			$context = ['{url}' => $request->getUri()->__toString(), '{err}' => $curl->getErrorMessage()];
			
			throw new CurlClientException(\strtr($message, $context), $curl->getErrorCode());
		}
		
		$response = $headerHandler->applyTo($response);
		
		return $bodyHandler->applyTo($response);
	}
	
}
