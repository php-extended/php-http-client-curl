<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;

/**
 * CurlClientException class file.
 * 
 * This exception is thrown when the curl client has a problem making an http
 * request.
 * 
 * @author Anastaszor
 */
class CurlClientException extends RuntimeException implements ClientExceptionInterface
{
	
	// nothing to add
	
}
