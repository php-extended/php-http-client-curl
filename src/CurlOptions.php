<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateTimeInterface;
use Exception;
use PhpExtended\Curl\CurlCompositeHeaderFunction;
use PhpExtended\Curl\CurlCompositePasswordFunction;
use PhpExtended\Curl\CurlCompositeProgressFunction;
use PhpExtended\Curl\CurlCompositeReadFunction;
use PhpExtended\Curl\CurlCompositeWriteFunction;
use PhpExtended\Curl\CurlFtpFileMethodInterface;
use PhpExtended\Curl\CurlFtpSslAuthMethodInterface;
use PhpExtended\Curl\CurlHeaderFunctionInterface;
use PhpExtended\Curl\CurlHeaderOptionInterface;
use PhpExtended\Curl\CurlHttpAuthMethodInterface;
use PhpExtended\Curl\CurlHttpVersionInterface;
use PhpExtended\Curl\CurlInterface;
use PhpExtended\Curl\CurlIpResolveMethodInterface;
use PhpExtended\Curl\CurlPasswordFunctionInterface;
use PhpExtended\Curl\CurlPostRedirectionInterface;
use PhpExtended\Curl\CurlProgressFunctionInterface;
use PhpExtended\Curl\CurlProtocolInterface;
use PhpExtended\Curl\CurlProxyTypeInterface;
use PhpExtended\Curl\CurlReadFunctionInterface;
use PhpExtended\Curl\CurlSharedInterface;
use PhpExtended\Curl\CurlSslAuthTypeInterface;
use PhpExtended\Curl\CurlSslKeyTypeInterface;
use PhpExtended\Curl\CurlSslOptionInterface;
use PhpExtended\Curl\CurlSslVerifyHostMethodInterface;
use PhpExtended\Curl\CurlSslVersionInterface;
use PhpExtended\Curl\CurlTimeConditionInterface;
use PhpExtended\Curl\CurlWriteFunctionInterface;
use PhpExtended\Ip\Ipv4AddressInterface;
use PhpExtended\Ip\Ipv6AddressInterface;
use PhpExtended\UserAgent\UserAgentInterface;
use Stringable;

/**
 * CurlOptions class file.
 * 
 * This class represents all the available options to use with the curl library.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/function.curl-setopt.php
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
 * @SuppressWarnings("PHPMD.ElseExpression")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.NPathComplexity")
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class CurlOptions implements Stringable
{
	
	/**
	 * @var boolean
	 */
	protected bool $_autoreferer = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_cookieSession = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_certInfo = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_connectOnly = false;
	
	/**
	 * "Warning: curl_setopt(): CURLOPT_DNS_USE_GLOBAL_CACHE cannot be
	 * activated when thread safety is enabled" : so we disable it by default
	 * as it is not thread safe.
	 * 
	 * @var boolean
	 */
	protected bool $_dnsUseGlobalCache = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_sslFalseStart = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_fileTime = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_followLocation = true;
	
	/**
	 * @var boolean
	 */
	protected bool $_forbidReuse = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_freshConnect = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_ftpUseEprt = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_ftpUseEpsv = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_ftpCreateMissingDirs = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_ftpAppend = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_tcpNoDelay = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_ftpListOnly = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_httpProxyTunnel = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_netrc = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_noProgress = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_pathAsIs = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_pipeWait = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_saslIr = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_sslAlpn = true;
	
	/**
	 * @var boolean
	 */
	protected bool $_sslNpn = true;
	
	/**
	 * @var boolean
	 */
	protected bool $_sslVerifyPeer = true;
	
	/**
	 * @var boolean
	 */
	protected bool $_sslVerifyStatus = true;
	
	/**
	 * @var boolean
	 */
	protected bool $_tcpFastOpen = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_tftpNoOptions = true;
	
	/**
	 * @var boolean
	 */
	protected bool $_transferText = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_unrestrictedAuth = false;
	
	/**
	 * @var boolean
	 */
	protected bool $_verbose = false;
	
	/**
	 * @var integer
	 */
	protected int $_bufferSize = 8388608;        // 8M
	
	/**
	 * @var integer
	 */
	protected int $_connectionTimeout = 0;       // no timeout
	
	/**
	 * @var integer
	 */
	protected int $_connectionTimeoutMs = 0;    // no timeout
	
	/**
	 * @var integer
	 */
	protected int $_dnsCacheTimeout = 120;      // 2 min
	
	/**
	 * @var integer
	 */
	protected int $_expect100TimeoutMs = 1000; // 1000 ms
	
	/**
	 * @var ?CurlFtpSslAuthMethodInterface
	 */
	protected ?CurlFtpSslAuthMethodInterface $_ftpSslAuth = null;
	
	/**
	 * @var ?CurlHeaderOptionInterface
	 */
	protected ?CurlHeaderOptionInterface $_headerOpt = null;
	
	/**
	 * @var ?CurlHttpVersionInterface
	 */
	protected ?CurlHttpVersionInterface $_httpVersion = null;
	
	/**
	 * @var ?CurlHttpAuthMethodInterface
	 */
	protected ?CurlHttpAuthMethodInterface $_httpAuth = null;
	
	/**
	 * @var integer
	 */
	protected int $_lowSpeedLimit = 0; // no limit
	
	/**
	 * @var integer
	 */
	protected int $_lowSpeedTime = 0;  // no limit
	
	/**
	 * @var integer
	 */
	protected int $_maxConnects = 0;    // no limit
	
	/**
	 * @var integer
	 */
	protected int $_maxRedirs = 20;
	
	/**
	 * @var ?integer
	 */
	protected ?int $_port = null;
	
	/**
	 * @var ?CurlPostRedirectionInterface
	 */
	protected ?CurlPostRedirectionInterface $_postRedir = null;
	
	/**
	 * @var ?CurlProtocolInterface
	 */
	protected ?CurlProtocolInterface $_protocol = null;
	
	/**
	 * @var ?CurlHttpAuthMethodInterface
	 */
	protected ?CurlHttpAuthMethodInterface $_proxyAuth = null;
	
	/**
	 * @var ?integer
	 */
	protected ?int $_proxyPort = null;
	
	/**
	 * @var ?CurlProxyTypeInterface
	 */
	protected ?CurlProxyTypeInterface $_proxyType = null;
	
	/**
	 * @var ?CurlProtocolInterface
	 */
	protected ?CurlProtocolInterface $_redirProtocol = null;
	
	/**
	 * @var ?integer
	 */
	protected ?int $_resumeFrom = null;
	
	/**
	 * @var ?CurlSslOptionInterface
	 */
	protected ?CurlSslOptionInterface $_sslOptions = null;
	
	/**
	 * @var ?CurlSslVerifyHostMethodInterface
	 */
	protected ?CurlSslVerifyHostMethodInterface $_sslVerifyHost = null;
	
	/**
	 * @var ?CurlSslVersionInterface
	 */
	protected ?CurlSslVersionInterface $_sslVersion = null;
	
	/**
	 * @var ?integer
	 */
	protected ?int $_streamWeight = null;
	
	/**
	 * @var ?CurlTimeConditionInterface
	 */
	protected ?CurlTimeConditionInterface $_timeCondition = null;
	
	/**
	 * @var integer
	 */
	protected int $_timeout = 0;    // no limit
	
	/**
	 * @var integer
	 */
	protected int $_timeoutMs = 0; // no limit
	
	/**
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_timeValue = null;
	
	/**
	 * @var integer
	 */
	protected int $_maxRecvSpeedLarge = 0; // no limit
	
	/**
	 * @var integer
	 */
	protected int $_maxSendSpeedLarge = 0; // no limit
	
	/**
	 * @var ?CurlSslAuthTypeInterface
	 */
	protected ?CurlSslAuthTypeInterface $_sslAuthType = null;
	
	/**
	 * @var ?CurlIpResolveMethodInterface
	 */
	protected ?CurlIpResolveMethodInterface $_ipResolve = null;
	
	/**
	 * @var ?CurlFtpFileMethodInterface
	 */
	protected ?CurlFtpFileMethodInterface $_ftpFileMethod = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_caInfo = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_caPath = null;
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_cookie = [];
	
	/**
	 * @var ?string
	 */
	protected ?string $_cookieFile = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_cookieJarFile = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_customRequest = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_defaultProtocol = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_dnsInterface = null;
	
	/**
	 * @var ?Ipv4AddressInterface
	 */
	protected ?Ipv4AddressInterface $_dnsLocalIp4 = null;
	
	/**
	 * @var ?Ipv6AddressInterface
	 */
	protected ?Ipv6AddressInterface $_dnsLocalIp6 = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_egdSocket = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_encoding = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_ftpPort = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_interface = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_keyPassword = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_krb4Level = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_loginOptions = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_pinnedPublicKey = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_postFields = null;
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_private = [];
	
	/**
	 * @var ?string
	 */
	protected ?string $_proxy = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_proxyServiceName = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_proxyUserPassword = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_randomFile = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_range = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_referer = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_serviceName = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sshHostPublicKeyMd5 = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sshPublicKeyFile = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sshPrivateKeyFile = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sslCipherList = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sslCert = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sslCertPassword = null;
	
	/**
	 * @var ?CurlSslKeyTypeInterface
	 */
	protected ?CurlSslKeyTypeInterface $_sslCertType = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sslEngine = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sslEngineDefault = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sslKey = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_sslKeyPassword = null;
	
	/**
	 * @var ?CurlSslKeyTypeInterface
	 */
	protected ?CurlSslKeyTypeInterface $_sslKeyType = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_unixSocketPath = null;
	
	/**
	 * @var ?UserAgentInterface
	 */
	protected ?UserAgentInterface $_userAgent = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_username = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_userPassword = null;
	
	/**
	 * @var ?string
	 */
	protected ?string $_xoAuthH2Bearer = null;
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_connectTos = [];
	
	/**
	 * @var array<integer, integer>
	 */
	protected array $_http200Aliases = [];
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_httpHeaders = [];
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_proxyHeaders = [];
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_postQuote = [];
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_quote = [];
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_resolve = [];
	
	/**
	 * @var ?resource
	 */
	protected $_fileStdout;
	
	/**
	 * @var ?resource
	 */
	protected $_fileStdin;
	
	/**
	 * @var ?resource
	 */
	protected $_fileStderr;
	
	/**
	 * @var ?resource
	 */
	protected $_fileHeader;
	
	/**
	 * @var ?CurlHeaderFunctionInterface
	 */
	protected ?CurlHeaderFunctionInterface $_headerFunction = null;
	
	/**
	 * @var ?CurlPasswordFunctionInterface
	 */
	protected ?CurlPasswordFunctionInterface $_passwordFunction = null;
	
	/**
	 * @var ?CurlProgressFunctionInterface
	 */
	protected ?CurlProgressFunctionInterface $_progressFunction = null;
	
	/**
	 * @var ?CurlReadFunctionInterface
	 */
	protected ?CurlReadFunctionInterface $_readFunction = null;
	
	/**
	 * @var ?CurlWriteFunctionInterface
	 */
	protected ?CurlWriteFunctionInterface $_writeFunction = null;
	
	/**
	 * @var ?CurlSharedInterface
	 */
	protected ?CurlSharedInterface $_sharedHandle = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets autoreferer value.
	 * 
	 * @return boolean
	 */
	public function hasAutoreferer() : bool
	{
		return $this->_autoreferer;
	}
	
	/**
	 * TRUE to automatically set the Referer: field in requests where it
	 * follows a Location: redirect. 
	 * 
	 * @param boolean $autoreferer
	 */
	public function setAutoreferer(bool $autoreferer) : void
	{
		$this->_autoreferer = $autoreferer;
	}
	
	/**
	 * Gets cookie session value.
	 * 
	 * @return boolean
	 */
	public function hasCookieSession() : bool
	{
		return $this->_cookieSession;
	}
	
	/**
	 * TRUE to mark this as a new cookie "session". It will force libcurl to
	 * ignore all cookies it is about to load that are "session cookies" from
	 * the previous session. By default, libcurl always stores and loads all
	 * cookies, independent if they are session cookies or not. Session cookies
	 * are cookies without expiry date and they are meant to be alive and
	 * existing for this "session" only. 
	 * 
	 * @param boolean $cookieSession
	 */
	public function setCookieSession(bool $cookieSession) : void
	{
		$this->_cookieSession = $cookieSession;
	}
	
	/**
	 * Gets the cert info value.
	 * 
	 * @return boolean
	 */
	public function hasCertInfo() : bool
	{
		return $this->_certInfo;
	}
	
	/**
	 * TRUE to output SSL certification information to STDERR on secure transfers.
	 * 
	 * @param boolean $certInfo
	 */
	public function setCertInfo(bool $certInfo) : void
	{
		$this->_certInfo = $certInfo;
	}
	
	/**
	 * Gets connect only value.
	 * 
	 * @return boolean
	 */
	public function hasConnectOnly() : bool
	{
		return $this->_connectOnly;
	}
	
	/**
	 * TRUE tells the library to perform all the required proxy authentication
	 * and connection setup, but no data transfer. This option is implemented
	 * for HTTP, SMTP and POP3. 
	 * 
	 * @param boolean $connectOnly
	 */
	public function setConnectOnly(bool $connectOnly) : void
	{
		$this->_connectOnly = $connectOnly;
	}
	
	/**
	 * Gets dns use global cache value.
	 * 
	 * @return boolean
	 */
	public function hasDnsUseGlobalCache() : bool
	{
		return $this->_dnsUseGlobalCache;
	}
	
	/**
	 * TRUE to use a global DNS cache. This option is not thread-safe and is
	 * enabled by default. 
	 * 
	 * @param boolean $dnsUseGlobalCache
	 */
	public function setDnsUseGlobalCache(bool $dnsUseGlobalCache) : void
	{
		$this->_dnsUseGlobalCache = $dnsUseGlobalCache;
	}
	
	/**
	 * Gets ssl false start value.
	 * 
	 * @return boolean
	 */
	public function hasSslFalseStart() : bool
	{
		return $this->_sslFalseStart;
	}
	
	/**
	 * TRUE to enable TLS false start.
	 * 
	 * @param boolean $sslFalseStart
	 */
	public function setSslFalseStart(bool $sslFalseStart) : void
	{
		$this->_sslFalseStart = $sslFalseStart;
	}
	
	/**
	 * Gets file time value.
	 * 
	 * @return boolean
	 */
	public function hasFileTime() : bool
	{
		return $this->_fileTime;
	}
	
	/**
	 * TRUE to attempt to retrieve the modification date of the remote document.
	 * This value can be retrieved using the CURLINFO_FILETIME option with
	 * curl_getinfo(). 
	 * 
	 * @param boolean $fileTime
	 */
	public function setFileTime(bool $fileTime) : void
	{
		$this->_fileTime = $fileTime;
	}
	
	/**
	 * Gets follow location value.
	 * 
	 * @return boolean
	 */
	public function hasFollowLocation() : bool
	{
		return $this->_followLocation;
	}
	
	/**
	 * TRUE to follow any "Location: " header that the server sends as part of
	 * the HTTP header (note this is recursive, PHP will follow as many
	 * "Location: " headers that it is sent, unless CURLOPT_MAXREDIRS is set). 
	 * 
	 * @param boolean $followLocation
	 */
	public function setFollowLocation(bool $followLocation) : void
	{
		$this->_followLocation = $followLocation;
	}
	
	/**
	 * Gets forbid reuse value.
	 * 
	 * @return boolean
	 */
	public function hasForbidReuse() : bool
	{
		return $this->_forbidReuse;
	}
	
	/**
	 * TRUE to force the connection to explicitly close when it has finished
	 * processing, and not be pooled for reuse. 
	 * 
	 * @param boolean $forbidReuse
	 */
	public function setForbidReuse(bool $forbidReuse) : void
	{
		$this->_forbidReuse = $forbidReuse;
	}
	
	/**
	 * Gets fresh connect value.
	 * 
	 * @return boolean
	 */
	public function hasFreshConnect() : bool
	{
		return $this->_freshConnect;
	}
	
	/**
	 * TRUE to force the use of a new connection instead of a cached one.
	 * 
	 * @param boolean $freshConnect
	 */
	public function setFreshConnect(bool $freshConnect) : void
	{
		$this->_freshConnect = $freshConnect;
	}
	
	/**
	 * Gets ftp use eprt value.
	 * 
	 * @return boolean
	 */
	public function hasFtpUseEprt() : bool
	{
		return $this->_ftpUseEprt;
	}
	
	/**
	 * TRUE to use EPRT (and LPRT) when doing active FTP downloads. Use FALSE
	 * to disable EPRT and LPRT and use PORT only. 
	 * 
	 * @param boolean $ftpUseEprt
	 */
	public function setFtpUseEprt(bool $ftpUseEprt) : void
	{
		$this->_ftpUseEprt = $ftpUseEprt;
	}
	
	/**
	 * Gets ftp use epsv value.
	 * 
	 * @return boolean
	 */
	public function hasFtpUseEpsv() : bool
	{
		return $this->_ftpUseEpsv;
	}
	
	/**
	 * TRUE to first try an EPSV command for FTP transfers before reverting
	 * back to PASV. Set to FALSE to disable EPSV. 
	 * 
	 * @param boolean $ftpUseEpsv
	 */
	public function setFtpUseEpsv(bool $ftpUseEpsv) : void
	{
		$this->_ftpUseEpsv = $ftpUseEpsv;
	}
	
	/**
	 * Gets ftp create missing dirs value.
	 * 
	 * @return boolean
	 */
	public function hasFtpCreateMissingDirs() : bool
	{
		return $this->_ftpCreateMissingDirs;
	}
	
	/**
	 * TRUE to create missing directories when an FTP operation encounters a
	 * path that currently doesn't exist. 
	 * 
	 * @param boolean $ftpCreateMissingDirs
	 */
	public function setFtpCreateMissingDirs(bool $ftpCreateMissingDirs) : void
	{
		$this->_ftpCreateMissingDirs = $ftpCreateMissingDirs;
	}
	
	/**
	 * Gets ftp append value.
	 * 
	 * @return boolean
	 */
	public function hasFtpAppend() : bool
	{
		return $this->_ftpAppend;
	}
	
	/**
	 * TRUE to append to the remote file instead of overwriting it.
	 * 
	 * @param boolean $ftpAppend
	 */
	public function setFtpAppend(bool $ftpAppend) : void
	{
		$this->_ftpAppend = $ftpAppend;
	}
	
	/**
	 * Gets tcp no delay value.
	 * 
	 * @return boolean
	 */
	public function hasTcpNoDelay() : bool
	{
		return $this->_tcpNoDelay;
	}
	
	/**
	 * TRUE to disable TCP's Nagle algorithm, which tries to minimize the 
	 * number of small packets on the network. 
	 * 
	 * @param boolean $tcpNoDelay
	 */
	public function setTcpNoDelay(bool $tcpNoDelay) : void
	{
		$this->_tcpNoDelay = $tcpNoDelay;
	}
	
	/**
	 * Gets ftp list only value.
	 * 
	 * @return boolean
	 */
	public function hasFtpListOnly() : bool
	{
		return $this->_ftpListOnly;
	}
	
	/**
	 * TRUE to only list the names of an FTP directory. 
	 * 
	 * @param boolean $ftpListOnly
	 */
	public function setFtpListOnly(bool $ftpListOnly) : void
	{
		$this->_ftpListOnly = $ftpListOnly;
	}
	
	/**
	 * Gets http proxy tunnel value.
	 * 
	 * @return boolean
	 */
	public function hasHttpProxyTunnel() : bool
	{
		return $this->_httpProxyTunnel;
	}
	
	/**
	 * TRUE to tunnel through a given HTTP proxy.
	 * 
	 * @param boolean $httpProxyTunnel
	 */
	public function setHttpProxyTunnel(bool $httpProxyTunnel) : void
	{
		$this->_httpProxyTunnel = $httpProxyTunnel;
	}
	
	/**
	 * Gets the netrc value.
	 * 
	 * @return boolean
	 */
	public function hasNetrc() : bool
	{
		return $this->_netrc;
	}
	
	/**
	 * TRUE to scan the ~/.netrc file to find a username and password for the
	 * remote site that a connection is being established with. 
	 * 
	 * @param boolean $netrc
	 */
	public function setNetrc($netrc) : void
	{
		$this->_netrc = $netrc;
	}
	
	/**
	 * Gets the no progress value.
	 * 
	 * @return boolean
	 */
	public function hasNoProgress() : bool
	{
		return $this->_noProgress;
	}
	
	/**
	 * TRUE to disable the progress meter for cURL transfers.
	 * 
	 * @param boolean $noProgress
	 */
	public function setNoProgress(bool $noProgress) : void
	{
		$this->_noProgress = $noProgress;
	}
	
	// TODO no signal ? CURLOPT_NOSIGNAL
	
	/**
	 * Gets path as is value.
	 * 
	 * @return boolean
	 */
	public function hasPathAsIs() : bool
	{
		return $this->_pathAsIs;
	}
	
	/**
	 * TRUE to not handle dot dot sequences.
	 * 
	 * @param boolean $pathAsIs
	 */
	public function setPathAsIs(bool $pathAsIs) : void
	{
		$this->_pathAsIs = $pathAsIs;
	}
	
	/**
	 * Gets pipe wait value.
	 * 
	 * @return boolean
	 */
	public function hasPipeWait() : bool
	{
		return $this->_pipeWait;
	}
	
	/**
	 * TRUE to wait for pipelining/multiplexing.
	 * 
	 * @param boolean $pipeWait
	 */
	public function setPipeWait(bool $pipeWait) : void
	{
		$this->_pipeWait = $pipeWait;
	}
	
	/**
	 * Gets sasl ir value.
	 * 
	 * @return boolean
	 */
	public function hasSaslIr() : bool
	{
		return $this->_saslIr;
	}
	
	/**
	 * TRUE to enable sending the initial response in the first packet. 
	 * 
	 * @param boolean $saslIr
	 */
	public function setSaslIr(bool $saslIr) : void
	{
		$this->_saslIr = $saslIr;
	}
	
	/**
	 * Gets ssl alpn value.
	 * 
	 * @return boolean
	 */
	public function hasSslAlpn() : bool
	{
		return $this->_sslAlpn;
	}
	
	/**
	 * FALSE to disable ALPN in the SSL handshake (if the SSL backend libcurl
	 * is built to use supports it), which can be used to negotiate http2. 
	 * 
	 * @param boolean $sslAlpn
	 */
	public function setSslAlpn(bool $sslAlpn) : void
	{
		$this->_sslAlpn = $sslAlpn;
	}
	
	/**
	 * Gets ssl npn value.
	 * 
	 * @return boolean
	 */
	public function hasSslNpn() : bool
	{
		return $this->_sslNpn;
	}
	
	/**
	 * FALSE to disable NPN in the SSL handshake (if the SSL backend libcurl is
	 * built to use supports it), which can be used to negotiate http2. 
	 * 
	 * @param boolean $sslNpn
	 */
	public function setSslNpn(bool $sslNpn) : void
	{
		$this->_sslNpn = $sslNpn;
	}
	
	/**
	 * Gets ssl verify peer value.
	 * 
	 * @return boolean
	 */
	public function hasSslVerifyPeer() : bool
	{
		return $this->_sslVerifyPeer;
	}
	
	/**
	 * FALSE to stop cURL from verifying the peer's certificate. Alternate
	 * certificates to verify against can be specified with the CURLOPT_CAINFO
	 * option or a certificate directory can be specified with the
	 * CURLOPT_CAPATH option.
	 * 
	 * @param boolean $sslVerifyPeer
	 */
	public function setSslVerifyPeer(bool $sslVerifyPeer) : void
	{
		$this->_sslVerifyPeer = $sslVerifyPeer;
	}
	
	/**
	 * Gets ssl verify status value.
	 * 
	 * @return boolean
	 */
	public function hasSslVerifyStatus() : bool
	{
		return $this->_sslVerifyStatus;
	}
	
	/**
	 * TRUE to verify the certificate's status. 
	 * 
	 * @param boolean $sslVerifyStatus
	 */
	public function setSslVerifyStatus(bool $sslVerifyStatus) : void
	{
		$this->_sslVerifyStatus = $sslVerifyStatus;
	}
	
	/**
	 * Gets tcp fast open value.
	 * 
	 * @return boolean
	 */
	public function hasTcpFastOpen() : bool
	{
		return $this->_tcpFastOpen;
	}
	
	/**
	 * TRUE to enable TCP Fast Open. 
	 * 
	 * @param boolean $tcpFastOpen
	 */
	public function setTcpFastOpen(bool $tcpFastOpen) : void
	{
		$this->_tcpFastOpen = $tcpFastOpen;
	}
	
	/**
	 * Gets tftp no options value.
	 * 
	 * @return boolean
	 */
	public function hasTftpNoOptions() : bool
	{
		return $this->_tftpNoOptions;
	}
	
	/**
	 * TRUE to not send TFTP options requests.
	 * 
	 * @param boolean $tftpNoOptions
	 */
	public function setTftpNoOptions(bool $tftpNoOptions) : void
	{
		$this->_tftpNoOptions = $tftpNoOptions;
	}
	
	/**
	 * Gets the transfer text value.
	 * 
	 * @return boolean
	 */
	public function hasTransferText() : bool
	{
		return $this->_transferText;
	}
	
	/**
	 * TRUE to use ASCII mode for FTP transfers. For LDAP, it retrieves data
	 * in plain text instead of HTML. On Windows systems, it will not set
	 * STDOUT to binary mode. 
	 * 
	 * @param boolean $transferText
	 */
	public function setTransferText(bool $transferText) : void
	{
		$this->_transferText = $transferText;
	}
	
	/**
	 * Gets unrestricted auth value.
	 * 
	 * @return boolean
	 */
	public function hasUnrestrictedAuth() : bool
	{
		return $this->_unrestrictedAuth;
	}
	
	/**
	 * TRUE to keep sending the username and password when following locations
	 * (using CURLOPT_FOLLOWLOCATION), even when the hostname has changed. 
	 * 
	 * @param boolean $unrestrictedAuth
	 */
	public function setUnrestrictedAuth(bool $unrestrictedAuth) : void
	{
		$this->_unrestrictedAuth = $unrestrictedAuth;
	}
	
	// TODO curl upload ?
	
	/**
	 * Gets verbose value.
	 * 
	 * @return boolean
	 */
	public function isVerbose() : bool
	{
		return $this->_verbose;
	}
	
	/**
	 * TRUE to output verbose information. Writes output to STDERR, or the file
	 * specified using CURLOPT_STDERR.
	 * 
	 * @param boolean $verbose
	 */
	public function setVerbose(bool $verbose) : void
	{
		$this->_verbose = $verbose;
	}
	
	/**
	 * Gets the buffer size value.
	 * 
	 * @return integer
	 */
	public function getBufferSize() : int
	{
		return $this->_bufferSize;
	}
	
	/**
	 * The size of the buffer to use for each read. There is no guarantee this
	 * request will be fulfilled, however. 
	 * 
	 * @param integer $bufferSize
	 */
	public function setBufferSize(int $bufferSize) : void
	{
		$this->_bufferSize = $bufferSize;
	}
	
	/**
	 * Gets the connection timeout value.
	 * 
	 * @return integer
	 */
	public function getConnectionTimeout() : int
	{
		return $this->_connectionTimeout;
	}
	
	/**
	 * The number of seconds to wait while trying to connect. Use 0 to wait
	 * indefinitely. 
	 * 
	 * @param integer $connectionTimeout
	 */
	public function setConnectionTimeout(int $connectionTimeout) : void
	{
		$this->_connectionTimeout = $connectionTimeout;
	}
	
	/**
	 * Gets the connection timeout ms.
	 * 
	 * @return integer
	 */
	public function getConnectionTimeoutMs() : int
	{
		return $this->_connectionTimeoutMs;
	}
	
	/**
	 * The number of milliseconds to wait while trying to connect. Use 0 to
	 * wait indefinitely. If libcurl is built to use the standard system name
	 * resolver, that portion of the connect will still use full-second
	 * resolution for timeouts with a minimum timeout allowed of one second.
	 * 
	 * @param integer $connectionTimeoutMs
	 */
	public function setConnectionTimeoutMs(int $connectionTimeoutMs) : void
	{
		$this->_connectionTimeoutMs = $connectionTimeoutMs;
	}
	
	/**
	 * Gets dns cache timeout value.
	 * 
	 * @return integer
	 */
	public function getDnsCacheTimeout() : int
	{
		return $this->_dnsCacheTimeout;
	}
	
	/**
	 * The number of seconds to keep DNS entries in memory. This option is set
	 * to 120 (2 minutes) by default. 
	 * 
	 * @param integer $dnsCacheTimeout
	 */
	public function setDnsCacheTimeout(int $dnsCacheTimeout) : void
	{
		$this->_dnsCacheTimeout = $dnsCacheTimeout;
	}
	
	/**
	 * Gets expect 100 timeout ms value.
	 * 
	 * @return integer
	 */
	public function getExpect100TimeoutMs() : int
	{
		return $this->_expect100TimeoutMs;
	}
	
	/**
	 * The timeout for Expect: 100-continue responses in milliseconds. Defaults
	 * to 1000 milliseconds. 
	 * 
	 * @param integer $expect100TimeoutMs
	 */
	public function setExpect100TimeoutMs(int $expect100TimeoutMs) : void
	{
		$this->_expect100TimeoutMs = $expect100TimeoutMs;
	}
	
	/**
	 * Gets ftp ssl auth value.
	 * 
	 * @return ?CurlFtpSslAuthMethodInterface
	 */
	public function getFtpSslAuth() : ?CurlFtpSslAuthMethodInterface
	{
		return $this->_ftpSslAuth;
	}
	
	/**
	 * The FTP authentication method (when is activated): CURLFTPAUTH_SSL
	 * (try SSL first), CURLFTPAUTH_TLS (try TLS first), or CURLFTPAUTH_DEFAULT
	 * (let cURL decide). 
	 * 
	 * @param CurlFtpSslAuthMethodInterface $ftpSslAuth
	 */
	public function setFtpSslAuth(CurlFtpSslAuthMethodInterface $ftpSslAuth) : void
	{
		$this->_ftpSslAuth = $ftpSslAuth;
	}
	
	/**
	 * Gets the header opt value.
	 * 
	 * @return ?CurlHeaderOptionInterface
	 */
	public function getHeaderOpt() : ?CurlHeaderOptionInterface
	{
		return $this->_headerOpt;
	}
	
	/**
	 * How to deal with headers. One of the following constants:
	 * CURLHEADER_UNIFIED: the headers specified in CURLOPT_HTTPHEADER will be
	 * 		used in requests both to servers and proxies. With this option
	 * 		enabled, CURLOPT_PROXYHEADER will not have any effect.
	 * CURLHEADER_SEPARATE: makes CURLOPT_HTTPHEADER headers only get sent to
	 * 		a server and not to a proxy. Proxy headers must be set with
	 * 		CURLOPT_PROXYHEADER to get used. Note that if a non-CONNECT
	 * 		request is sent to a proxy, libcurl will send both server headers
	 * 		and proxy headers. When doing CONNECT, libcurl will send 
	 * 		CURLOPT_PROXYHEADER headers only to the proxy and then 
	 * 		CURLOPT_HTTPHEADER headers only to the server.
	 * Defaults to CURLHEADER_SEPARATE as of cURL 7.42.1, and
	 * CURLHEADER_UNIFIED before. 
	 * 
	 * @param CurlHeaderOptionInterface $headerOpt
	 */
	public function setHeaderOpt(CurlHeaderOptionInterface $headerOpt) : void
	{
		$this->_headerOpt = $headerOpt;
	}
	
	/**
	 * Gets the http version value.
	 * 
	 * @return ?CurlHttpVersionInterface
	 */
	public function getHttpVersion() : ?CurlHttpVersionInterface
	{
		return $this->_httpVersion;
	}
	
	/**
	 * CURL_HTTP_VERSION_NONE (default, lets CURL decide which version to
	 * use), CURL_HTTP_VERSION_1_0 (forces HTTP/1.0), or CURL_HTTP_VERSION_1_1
	 * (forces HTTP/1.1). 
	 * 
	 * @param CurlHttpVersionInterface $httpVersion
	 */
	public function setHttpVersion(CurlHttpVersionInterface $httpVersion) : void
	{
		$this->_httpVersion = $httpVersion;
	}
	
	/**
	 * Gets http auth value.
	 * 
	 * @return ?CurlHttpAuthMethodInterface
	 */
	public function getHttpAuth() : ?CurlHttpAuthMethodInterface
	{
		return $this->_httpAuth;
	}
	
	/**
	 * The HTTP authentication method(s) to use. The options are: CURLAUTH_BASIC,
	 * CURLAUTH_DIGEST, CURLAUTH_GSSNEGOTIATE, CURLAUTH_NTLM, CURLAUTH_ANY, and
	 * CURLAUTH_ANYSAFE.
	 * 
	 * The bitwise | (or) operator can be used to combine more than one method.
	 * If this is done, cURL will poll the server to see what methods it
	 * supports and pick the best one.
	 * 
	 * CURLAUTH_ANY is an alias for CURLAUTH_BASIC | CURLAUTH_DIGEST | 
	 * CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM.
	 * 
	 * CURLAUTH_ANYSAFE is an alias for CURLAUTH_DIGEST | CURLAUTH_GSSNEGOTIATE
	 * | CURLAUTH_NTLM. 
	 * 
	 * @param CurlHttpAuthMethodInterface $httpAuth
	 */
	public function setHttpAuth(CurlHttpAuthMethodInterface $httpAuth) : void
	{
		$this->_httpAuth = $httpAuth;
	}
	
	/**
	 * Gets the low speed limit.
	 * 
	 * @return integer
	 */
	public function getLowSpeedLimit() : int
	{
		return $this->_lowSpeedLimit;
	}
	
	/**
	 * The transfer speed, in bytes per second, that the transfer should be
	 * below during the count of CURLOPT_LOW_SPEED_TIME seconds before PHP
	 * considers the transfer too slow and aborts. 
	 * 
	 * @param integer $lowSpeedLimit
	 */
	public function setLowSpeedLimit(int $lowSpeedLimit) : void
	{
		$this->_lowSpeedLimit = $lowSpeedLimit;
	}
	
	/**
	 * Gets low speed time value.
	 * 
	 * @return integer
	 */
	public function getLowSpeedTime() : int
	{
		return $this->_lowSpeedTime;
	}
	
	/**
	 * The number of seconds the transfer speed should be below
	 * CURLOPT_LOW_SPEED_LIMIT before PHP considers the transfer too slow and
	 * aborts. 	
	 * 
	 * @param integer $lowSpeedTime
	 */
	public function setLowSpeedTime(int $lowSpeedTime) : void
	{
		$this->_lowSpeedTime = $lowSpeedTime;
	}
	
	/**
	 * Gets the max connects value.
	 * 
	 * @return integer
	 */
	public function getMaxConnects() : int
	{
		return $this->_maxConnects;
	}
	
	/**
	 * The maximum amount of persistent connections that are allowed. When the
	 * limit is reached, CURLOPT_CLOSEPOLICY is used to determine which
	 * connection to close. 
	 * 
	 * @param integer $maxConnects
	 */
	public function setMaxConnects(int $maxConnects) : void
	{
		$this->_maxConnects = $maxConnects;
	}
	
	/**
	 * Gets the max redirs value.
	 * 
	 * @return integer
	 */
	public function getMaxRedirs() : int
	{
		return $this->_maxRedirs;
	}
	
	/**
	 * The maximum amount of HTTP redirections to follow. Use this option
	 * alongside CURLOPT_FOLLOWLOCATION. 
	 * 
	 * @param integer $maxRedirs
	 */
	public function setMaxRedirs(int $maxRedirs) : void
	{
		$this->_maxRedirs = $maxRedirs;
	}
	
	/**
	 * Gets the port value.
	 * 
	 * @return ?integer
	 */
	public function getPort() : ?int
	{
		return $this->_port;
	}
	
	/**
	 * An alternative port number to connect to.
	 * 
	 * @param integer $port
	 */
	public function setPort(int $port) : void
	{
		$this->_port = $port;
	}
	
	/**
	 * Gets the post redir value.
	 * 
	 * @return ?CurlPostRedirectionInterface
	 */
	public function getPostRedir() : ?CurlPostRedirectionInterface
	{
		return $this->_postRedir;
	}
	
	/**
	 * A bitmask of 1 (301 Moved Permanently), 2 (302 Found) and 4 (303 See
	 * Other) if the HTTP POST method should be maintained when 
	 * CURLOPT_FOLLOWLOCATION is set and a specific type of redirect occurs.
	 * 
	 * @param CurlPostRedirectionInterface $postRedir
	 */
	public function setPostRedir(CurlPostRedirectionInterface $postRedir) : void
	{
		$this->_postRedir = $postRedir;
	}
	
	/**
	 * Gets the protocol value.
	 * 
	 * @return ?CurlProtocolInterface
	 */
	public function getProtocol() : ?CurlProtocolInterface
	{
		return $this->_protocol;
	}
	
	/**
	 * Bitmask of CURLPROTO_* values. If used, this bitmask limits what
	 * protocols libcurl may use in the transfer. This allows you to have a
	 * libcurl built to support a wide range of protocols but still limit
	 * specific transfers to only be allowed to use a subset of them. By
	 * default libcurl will accept all protocols it supports. See also 
	 * CURLOPT_REDIR_PROTOCOLS.
	 * 
	 * Valid protocol options are: CURLPROTO_HTTP, CURLPROTO_HTTPS,
	 * CURLPROTO_FTP, CURLPROTO_FTPS, CURLPROTO_SCP, CURLPROTO_SFTP,
	 * CURLPROTO_TELNET, CURLPROTO_LDAP, CURLPROTO_LDAPS, CURLPROTO_DICT,
	 * CURLPROTO_FILE, CURLPROTO_TFTP, CURLPROTO_ALL
	 * 
	 * @param CurlProtocolInterface $protocol
	 */
	public function setProtocol(CurlProtocolInterface $protocol) : void
	{
		$this->_protocol = $protocol;
	}
	
	/**
	 * Gets the proxy auth value.
	 * 
	 * @return ?CurlHttpAuthMethodInterface
	 */
	public function getProxyAuth() : ?CurlHttpAuthMethodInterface
	{
		return $this->_proxyAuth;
	}
	
	/**
	 * The HTTP authentication method(s) to use for the proxy connection. Use
	 * the same bitmasks as described in CURLOPT_HTTPAUTH. For proxy
	 * authentication, only CURLAUTH_BASIC and CURLAUTH_NTLM are currently
	 * supported. 
	 * 
	 * @param CurlHttpAuthMethodInterface $proxyAuth
	 */
	public function setProxyAuth(CurlHttpAuthMethodInterface $proxyAuth) : void
	{
		$this->_proxyAuth = $proxyAuth;
	}
	
	/**
	 * Gets the proxy port value.
	 * 
	 * @return ?integer
	 */
	public function getProxyPort() : ?int
	{
		return $this->_proxyPort;
	}
	
	/**
	 * @param integer $port
	 */
	public function setProxyPort(int $port) : void
	{
		$this->_proxyPort = $port;
	}
	
	/**
	 * Gets the proxy type value.
	 * 
	 * @return ?CurlProxyTypeInterface
	 */
	public function getProxyType() : ?CurlProxyTypeInterface
	{
		return $this->_proxyType;
	}
	
	/**
	 * The port number of the proxy to connect to. This port number can also be
	 * set in CURLOPT_PROXY.
	 * 
	 * @param CurlProxyTypeInterface $proxyType
	 */
	public function setProxyType(CurlProxyTypeInterface $proxyType) : void
	{
		$this->_proxyType = $proxyType;
	}
	
	/**
	 * Gets the redir protocol value.
	 * 
	 * @return ?CurlProtocolInterface
	 */
	public function getRedirProtocol() : ?CurlProtocolInterface
	{
		return $this->_redirProtocol;
	}
	
	/**
	 * Bitmask of CURLPROTO_* values. If used, this bitmask limits what
	 * protocols libcurl may use in a transfer that it follows to in a redirect
	 * when CURLOPT_FOLLOWLOCATION is enabled. This allows you to limit
	 * specific transfers to only be allowed to use a subset of protocols in
	 * redirections. By default libcurl will allow all protocols except for
	 * FILE and SCP. This is a difference compared to pre-7.19.4 versions which
	 * unconditionally would follow to all protocols supported. See also
	 * CURLOPT_PROTOCOLS for protocol constant values. 
	 * 
	 * @param CurlProtocolInterface $redirProtocol
	 */
	public function setRedirProtocol(CurlProtocolInterface $redirProtocol) : void
	{
		$this->_redirProtocol = $redirProtocol;
	}
	
	/**
	 * Gets the resume from value.
	 * 
	 * @return ?integer
	 */
	public function getResumeFrom() : ?int
	{
		return $this->_resumeFrom;
	}
	
	/**
	 * The offset, in bytes, to resume a transfer from.
	 * 
	 * @param integer $resumeFrom
	 */
	public function setResumeFrom(int $resumeFrom) : void
	{
		$this->_resumeFrom = $resumeFrom;
	}
	
	/**
	 * Gets the ssl options value.
	 * 
	 * @return ?CurlSslOptionInterface
	 */
	public function getSslOptions() : ?CurlSslOptionInterface
	{
		return $this->_sslOptions;
	}
	
	/**
	 * Set SSL behavior options, which is a bitmask of any of the following
	 * constants:
	 * CURLSSLOPT_ALLOW_BEAST: do not attempt to use any workarounds for a
	 * 		security flaw in the SSL3 and TLS1.0 protocols.
	 * CURLSSLOPT_NO_REVOKE: disable certificate revocation checks for those
	 * 		SSL backends where such behavior is present. 
	 * 
	 * @param CurlSslOptionInterface $sslOptions
	 */
	public function setSslOptions(CurlSslOptionInterface $sslOptions) : void
	{
		$this->_sslOptions = $sslOptions;
	}
	
	/**
	 * Gets the ssl verify host value.
	 * 
	 * @return ?CurlSslVerifyHostMethodInterface
	 */
	public function getSslVerifyHost() : ?CurlSslVerifyHostMethodInterface
	{
		return $this->_sslVerifyHost;
	}
	
	/**
	 * 1 to check the existence of a common name in the SSL peer certificate.
	 * 2 to check the existence of a common name and also verify that it matches
	 * 		the hostname provided. 0 to not check the names. In production
	 * 		environments the value of this option should be kept at 2 (default
	 * 		value). 
	 * 
	 * @param CurlSslVerifyHostMethodInterface $sslVerifyHost
	 */
	public function setSslVerifyHost(CurlSslVerifyHostMethodInterface $sslVerifyHost) : void
	{
		$this->_sslVerifyHost = $sslVerifyHost;
	}
	
	/**
	 * Gets the ssl version value.
	 * 
	 * @return ?CurlSslVersionInterface
	 */
	public function getSslVersion() : ?CurlSslVersionInterface
	{
		return $this->_sslVersion;
	}
	
	/**
	 * One of CURL_SSLVERSION_DEFAULT (0), CURL_SSLVERSION_TLSv1 (1),
	 * CURL_SSLVERSION_SSLv2 (2), CURL_SSLVERSION_SSLv3 (3), 
	 * CURL_SSLVERSION_TLSv1_0 (4), CURL_SSLVERSION_TLSv1_1 (5) or 
	 * CURL_SSLVERSION_TLSv1_2 (6).
	 * 
	 * Note: Your best bet is to not set this and let it use the default.
	 * Setting it to 2 or 3 is very dangerous given the known vulnerabilities
	 * in SSLv2 and SSLv3.
	 * 
	 * @param CurlSslVersionInterface $sslVersion
	 */
	public function setSslVersion(CurlSslVersionInterface $sslVersion) : void
	{
		$this->_sslVersion = $sslVersion;
	}
	
	/**
	 * Gets the stream weight.
	 * 
	 * @return ?integer
	 */
	public function getStreamWeight() : ?int
	{
		return $this->_streamWeight;
	}
	
	/**
	 * Set the numerical stream weight (a number between 1 and 256).
	 * 
	 * @param integer $streamWeight
	 */
	public function setStreamWeight(int $streamWeight) : void
	{
		$this->_streamWeight = $streamWeight;
	}
	
	/**
	 * Gets the time condition value.
	 * 
	 * @return ?CurlTimeConditionInterface
	 */
	public function getTimeCondition() : ?CurlTimeConditionInterface
	{
		return $this->_timeCondition;
	}
	
	/**
	 * How CURLOPT_TIMEVALUE is treated. Use CURL_TIMECOND_IFMODSINCE to return
	 * the page only if it has been modified since the time specified in
	 * CURLOPT_TIMEVALUE. If it hasn't been modified, a "304 Not Modified"
	 * header will be returned assuming CURLOPT_HEADER is TRUE. Use 
	 * CURL_TIMECOND_IFUNMODSINCE for the reverse effect.
	 * CURL_TIMECOND_IFMODSINCE is the default.
	 * 
	 * @param CurlTimeConditionInterface $timeCondition
	 */
	public function setTimeCondition(CurlTimeConditionInterface $timeCondition) : void
	{
		$this->_timeCondition = $timeCondition;
	}
	
	/**
	 * Gets the timeout value.
	 * 
	 * @return integer
	 */
	public function getTimeout() : int
	{
		return $this->_timeout;
	}
	
	/**
	 * The maximum number of seconds to allow cURL functions to execute.
	 * 
	 * @param integer $timeout
	 */
	public function setTimeout(int $timeout) : void
	{
		$this->_timeout = $timeout;
	}
	
	/**
	 * Gets the timeout ms value.
	 * 
	 * @return integer
	 */
	public function getTimeoutMs() : int
	{
		return $this->_timeoutMs;
	}
	
	/**
	 * The maximum number of milliseconds to allow cURL functions to execute.
	 * If libcurl is built to use the standard system name resolver, that
	 * portion of the connect will still use full-second resolution for
	 * timeouts with a minimum timeout allowed of one second.
	 * 
	 * @param integer $timeoutMs
	 */
	public function setTimeoutMs(int $timeoutMs) : void
	{
		$this->_timeoutMs = $timeoutMs;
	}
	
	/**
	 * Gets the time value.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getTimeValue() : ?DateTimeInterface
	{
		return $this->_timeValue;
	}
	
	/**
	 * The time in seconds since January 1st, 1970. The time will be used by
	 * CURLOPT_TIMECONDITION. By default, CURL_TIMECOND_IFMODSINCE is used.
	 * 
	 * @param DateTimeInterface $timeValue
	 */
	public function setTimeValue(DateTimeInterface $timeValue) : void
	{
		$this->_timeValue = $timeValue;
	}
	
	/**
	 * Gets max recv speed large value.
	 * 
	 * @return integer
	 */
	public function getMaxRecvSpeedLarge() : int
	{
		return $this->_maxRecvSpeedLarge;
	}
	
	/**
	 * If a download exceeds this speed (counted in bytes per second) on 
	 * cumulative average during the transfer, the transfer will pause to keep
	 * the average rate less than or equal to the parameter value. Defaults
	 * to unlimited speed. 
	 * 
	 * @param integer $maxRecvSpeedLarge
	 */
	public function setMaxRecvSpeedLarge(int $maxRecvSpeedLarge) : void
	{
		$this->_maxRecvSpeedLarge = $maxRecvSpeedLarge;
	}
	
	/**
	 * Gets max send speed large value.
	 * 
	 * @return integer
	 */
	public function getMaxSendSpeedLarge() : int
	{
		return $this->_maxSendSpeedLarge;
	}
	
	/**
	 * If an upload exceeds this speed (counted in bytes per second) on
	 * cumulative average during the transfer, the transfer will pause to keep
	 * the average rate less than or equal to the parameter value. Defaults to
	 * unlimited speed. 
	 * 
	 * @param integer $maxSendSpeedLarge
	 */
	public function setMaxSendSpeedLarge(int $maxSendSpeedLarge) : void
	{
		$this->_maxSendSpeedLarge = $maxSendSpeedLarge;
	}
	
	/**
	 * Gets the ssl auth type value.
	 * 
	 * @return ?CurlSslAuthTypeInterface
	 */
	public function getSslAuthType() : ?CurlSslAuthTypeInterface
	{
		return $this->_sslAuthType;
	}
	
	/**
	 * A bitmask consisting of one or more of CURLSSH_AUTH_PUBLICKEY,
	 * CURLSSH_AUTH_PASSWORD, CURLSSH_AUTH_HOST, CURLSSH_AUTH_KEYBOARD. Set to
	 * CURLSSH_AUTH_ANY to let libcurl pick one. 
	 * 
	 * @param CurlSslAuthTypeInterface $sslAuthType
	 */
	public function setSslAuthType(CurlSslAuthTypeInterface $sslAuthType) : void
	{
		$this->_sslAuthType = $sslAuthType;
	}
	
	/**
	 * Gets the ip resolve value.
	 * 
	 * @return ?CurlIpResolveMethodInterface
	 */
	public function getIpResolve() : ?CurlIpResolveMethodInterface
	{
		return $this->_ipResolve;
	}
	
	/**
	 * Allows an application to select what kind of IP addresses to use when
	 * resolving host names. This is only interesting when using host names
	 * that resolve addresses using more than one version of IP, possible
	 * values are CURL_IPRESOLVE_WHATEVER, CURL_IPRESOLVE_V4, CURL_IPRESOLVE_V6,
	 * by default CURL_IPRESOLVE_WHATEVER. 
	 * 
	 * @param CurlIpResolveMethodInterface $ipResolve
	 */
	public function setIpResolve(CurlIpResolveMethodInterface $ipResolve) : void
	{
		$this->_ipResolve = $ipResolve;
	}
	
	/**
	 * Gets the ftp file method value.
	 * 
	 * @return ?CurlFtpFileMethodInterface
	 */
	public function getFtpFileMethod() : ?CurlFtpFileMethodInterface
	{
		return $this->_ftpFileMethod;
	}
	
	/**
	 * Tell curl which method to use to reach a file on a FTP(S) server.
	 * Possible values are CURLFTPMETHOD_MULTICWD, CURLFTPMETHOD_NOCWD and
	 * CURLFTPMETHOD_SINGLECWD. 
	 * 
	 * @param CurlFtpFileMethodInterface $ftpFileMethod
	 */
	public function setFtpFileMethod(CurlFtpFileMethodInterface $ftpFileMethod) : void
	{
		$this->_ftpFileMethod = $ftpFileMethod;
	}
	
	// TODO Ensure to string.
	
	/**
	 * Gets the ca info value.
	 * 
	 * @return ?string
	 */
	public function getCaInfo() : ?string
	{
		return $this->_caInfo;
	}
	
	/**
	 * The name of a file holding one or more certificates to verify the peer
	 * with. This only makes sense when used in combination with 
	 * CURLOPT_SSL_VERIFYPEER. 
	 * 
	 * @param string $caInfo
	 */
	public function setCaInfo(string $caInfo) : void
	{
		$this->_caInfo = $caInfo;
	}
	
	/**
	 * Gets the ca path value.
	 * 
	 * @return ?string
	 */
	public function getCaPath() : ?string
	{
		return $this->_caPath;
	}
	
	/**
	 * A directory that holds multiple CA certificates. Use this option
	 * alongside CURLOPT_SSL_VERIFYPEER. 
	 * 
	 * @param string $caPath
	 */
	public function setCaPath(string $caPath) : void
	{
		$this->_caPath = $caPath;
	}
	
	/**
	 * Gets the cookie value.
	 * 
	 * @return array<integer, string>
	 */
	public function getCookie() : array
	{
		return $this->_cookie;
	}
	
	/**
	 * The contents of the "Cookie: " header to be used in the HTTP request.
	 * Note that multiple cookies are separated with a semicolon followed by a
	 * space (e.g., "fruit=apple; colour=red").
	 * 
	 * @param array<integer|string, string> $cookie
	 */
	public function setCookie(array $cookie) : void
	{
		$this->_cookie = \array_values($cookie);
	}
	
	/**
	 * Gets the cookie file value.
	 * 
	 * @return ?string
	 */
	public function getCookieFile() : ?string
	{
		return $this->_cookieFile;
	}
	
	/**
	 * The name of the file containing the cookie data. The cookie file can be
	 * in Netscape format, or just plain HTTP-style headers dumped into a file.
	 * If the name is an empty string, no cookies are loaded, but cookie
	 * handling is still enabled. 
	 * 
	 * @param string $cookieFile
	 */
	public function setCookieFile(string $cookieFile) : void
	{
		$this->_cookieFile = $cookieFile;
	}
	
	/**
	 * Gets the cookie jar file value.
	 * 
	 * @return ?string
	 */
	public function getCookieJarFile() : ?string
	{
		return $this->_cookieJarFile;
	}
	
	/**
	 * The name of a file to save all internal cookies to when the handle is
	 * closed, e.g. after a call to curl_close. 
	 * 
	 * @param string $cookieJarFile
	 */
	public function setCookieJarFile(string $cookieJarFile) : void
	{
		$this->_cookieJarFile = $cookieJarFile;
	}
	
	/**
	 * Gets custom request value.
	 * 
	 * @return ?string
	 */
	public function getCustomRequest() : ?string
	{
		return $this->_customRequest;
	}
	
	/**
	 * A custom request method to use instead of "GET" or "HEAD" when doing a
	 * HTTP request. This is useful for doing "DELETE" or other, more obscure
	 * HTTP requests. Valid values are things like "GET", "POST", "CONNECT"
	 * and so on; i.e. Do not enter a whole HTTP request line here. For
	 * instance, entering "GET /index.html HTTP/1.0\r\n\r\n" would be incorrect.
	 * 
	 * Note: Don't do this without making sure the server supports the custom
	 * request method first.
	 * 
	 * @param string $customRequest
	 */
	public function setCustomRequest(string $customRequest) : void
	{
		$this->_customRequest = $customRequest;
	}
	
	/**
	 * Gets the default protocol value.
	 * 
	 * @return ?string
	 */
	public function getDefaultProtocol() : ?string
	{
		return $this->_defaultProtocol;
	}
	
	/**
	 * The default protocol to use if the URL is missing a scheme name.
	 * 
	 * @param string $defaultProtocol
	 */
	public function setDefaultProtocol(string $defaultProtocol) : void
	{
		$this->_defaultProtocol = $defaultProtocol;
	}
	
	/**
	 * Gets the dns interface value.
	 * 
	 * @return ?string
	 */
	public function getDnsInterface() : ?string
	{
		return $this->_dnsInterface;
	}
	
	/**
	 * Set the name of the network interface that the DNS resolver should bind
	 * to. This must be an interface name (not an address).
	 * 
	 * @param string $dnsInterface
	 */
	public function setDnsInterface(string $dnsInterface) : void
	{
		$this->_dnsInterface = $dnsInterface;
	}
	
	/**
	 * Gets the dns local ipv4 value.
	 * 
	 * @return ?Ipv4AddressInterface
	 */
	public function getDnsLocalIp4() : ?Ipv4AddressInterface
	{
		return $this->_dnsLocalIp4;
	}
	
	/**
	 * Set the local IPv4 address that the resolver should bind to. The
	 * argument should contain a single numerical IPv4 address as a string.
	 * 
	 * @param Ipv4AddressInterface $dnsLocalIp4
	 */
	public function setDnsLocalIp4(Ipv4AddressInterface $dnsLocalIp4) : void
	{
		$this->_dnsLocalIp4 = $dnsLocalIp4;
	}
	
	/**
	 * Gets the local dns ipv6.
	 * 
	 * @return ?Ipv6AddressInterface
	 */
	public function getDnsLocalIp6() : ?Ipv6AddressInterface
	{
		return $this->_dnsLocalIp6;
	}
	
	/**
	 * Set the local IPv6 address that the resolver should bind to. The
	 * argument should contain a single numerical IPv6 address as a string.
	 * 
	 * @param Ipv6AddressInterface $dnsLocalIp6
	 */
	public function setDnsLocalIp6(Ipv6AddressInterface $dnsLocalIp6) : void
	{
		$this->_dnsLocalIp6 = $dnsLocalIp6;
	}
	
	/**
	 * Gets the egd socket value.
	 * 
	 * @return ?string
	 */
	public function getEgdSocket() : ?string
	{
		return $this->_egdSocket;
	}
	
	/**
	 * Like CURLOPT_RANDOM_FILE, except a filename to an Entropy Gathering
	 * Daemon socket. 
	 * 
	 * @param string $egdSocket
	 */
	public function setEgdSocket(string $egdSocket) : void
	{
		$this->_egdSocket = $egdSocket;
	}
	
	/**
	 * Gets the encoding value.
	 * 
	 * @return ?string
	 */
	public function getEncoding() : ?string
	{
		return $this->_encoding;
	}
	
	/**
	 * The contents of the "Accept-Encoding: " header. This enables decoding
	 * of the response. Supported encodings are "identity", "deflate", and
	 * "gzip". If an empty string, "", is set, a header containing all
	 * supported encoding types is sent. 
	 * 
	 * @param string $encoding
	 */
	public function setEncoding(string $encoding) : void
	{
		$this->_encoding = $encoding;
	}
	
	/**
	 * Gets the ftp port value.
	 * 
	 * @return ?string
	 */
	public function getFtpPort() : ?string
	{
		return $this->_ftpPort;
	}
	
	/**
	 * The value which will be used to get the IP address to use for the FTP
	 * "PORT" instruction. The "PORT" instruction tells the remote server to
	 * connect to our specified IP address. The string may be a plain IP
	 * address, a hostname, a network interface name (under Unix), or just a
	 * plain '-' to use the systems default IP address. 
	 * 
	 * @param string $ftpPort
	 */
	public function setFtpPort(string $ftpPort) : void
	{
		$this->_ftpPort = $ftpPort;
	}
	
	/**
	 * Gets the interface value.
	 * 
	 * @return ?string
	 */
	public function getInterface() : ?string
	{
		return $this->_interface;
	}
	
	/**
	 * The name of the outgoing network interface to use. This can be an
	 * interface name, an IP address or a host name. 
	 * 
	 * @param string $interface
	 */
	public function setInterface(string $interface) : void
	{
		$this->_interface = $interface;
	}
	
	/**
	 * Gets the key password value.
	 * 
	 * @return ?string
	 */
	public function getKeyPassword() : ?string
	{
		return $this->_keyPassword;
	}
	
	/**
	 * The password required to use the CURLOPT_SSLKEY or 
	 * CURLOPT_SSH_PRIVATE_KEYFILE private key. 
	 * 
	 * @param string $keyPassword
	 */
	public function setKeyPassword(string $keyPassword) : void
	{
		$this->_keyPassword = $keyPassword;
	}
	
	/**
	 * Gets the kerberos 4 level value.
	 * 
	 * @return ?string
	 */
	public function getKrb4Level() : ?string
	{
		return $this->_krb4Level;
	}
	
	/**
	 * The KRB4 (Kerberos 4) security level. Any of the following values (in
	 * order from least to most powerful) are valid: "clear", "safe",
	 * "confidential", "private".. If the string does not match one of these,
	 * "private" is used. Setting this option to NULL will disable KRB4
	 * security. Currently KRB4 security only works with FTP transactions. 
	 * 
	 * @param string $krb4Level
	 */
	public function setKrb4Level(string $krb4Level) : void
	{
		$this->_krb4Level = $krb4Level;
	}
	
	/**
	 * Gets the login options value.
	 * 
	 * @return ?string
	 */
	public function getLoginOptions() : ?string
	{
		return $this->_loginOptions;
	}
	
	/**
	 * Can be used to set protocol specific login options, such as the
	 * preferred authentication mechanism via "AUTH=NTLM" or "AUTH=*", and
	 * should be used in conjunction with the CURLOPT_USERNAME option.
	 * 
	 * @param string $loginOptions
	 */
	public function setLoginOptions(string $loginOptions) : void
	{
		$this->_loginOptions = $loginOptions;
	}
	
	/**
	 * Gets the pinned public key value.
	 * 
	 * @return ?string
	 */
	public function getPinnedPublicKey() : ?string
	{
		return $this->_pinnedPublicKey;
	}
	
	/**
	 * Set the pinned public key. The string can be the file name of your
	 * pinned public key. The file format expected is "PEM" or "DER". The
	 * string can also be any number of base64 encoded sha256 hashes preceded
	 * by "sha256//" and separated by ";". 
	 * 
	 * @param string $pinnedPublicKey
	 */
	public function setPinnedPublicKey(string $pinnedPublicKey) : void
	{
		$this->_pinnedPublicKey = $pinnedPublicKey;
	}
	
	/**
	 * Gets the post fields value.
	 * 
	 * @return ?string
	 */
	public function getPostFields() : ?string
	{
		return $this->_postFields;
	}
	
	/**
	 * The full data to post in a HTTP "POST" operation. To post a file,
	 * prepend a filename with @ and use the full path. The filetype can be
	 * explicitly specified by following the filename with the type in the
	 * format ';type=mimetype'. This parameter can either be passed as a
	 * urlencoded string like 'para1=val1&para2=val2&...' or as an array with
	 * the field name as key and field data as value. If value is an array,
	 * the Content-Type header will be set to multipart/form-data. As of PHP
	 * 5.2.0, value must be an array if files are passed to this option with
	 * the @ prefix. As of PHP 5.5.0, the @ prefix is deprecated and files can
	 * be sent using CURLFile. The @ prefix can be disabled for safe passing
	 * of values beginning with @ by setting the CURLOPT_SAFE_UPLOAD option to
	 * TRUE. 
	 * 
	 * @param string $postFields
	 */
	public function setPostFields(string $postFields) : void
	{
		$this->_postFields = $postFields;
	}
	
	/**
	 * Gets the private value.
	 * 
	 * @return array<integer, string>
	 */
	public function getPrivate() : array
	{
		return $this->_private;
	}
	
	/**
	 * Any data that should be associated with this cURL handle. This data can
	 * subsequently be retrieved with the CURLINFO_PRIVATE option of
	 * curl_getinfo(). cURL does nothing with this data. When using a cURL
	 * multi handle, this private data is typically a unique key to identify a
	 * standard cURL handle. 
	 * 
	 * @param array<integer|string, string> $private
	 */
	public function setPrivate(array $private) : void
	{
		$this->_private = \array_values($private);
	}
	
	/**
	 * Gets the proxy value.
	 * 
	 * @return ?string
	 */
	public function getProxy() : ?string
	{
		return $this->_proxy;
	}
	
	/**
	 * The HTTP proxy to tunnel requests through.
	 * 
	 * @param string $proxy
	 */
	public function setProxy(string $proxy) : void
	{
		$this->_proxy = $proxy;
	}
	
	/**
	 * Gets the proxy service name.
	 * 
	 * @return ?string
	 */
	public function getProxyServiceName() : ?string
	{
		return $this->_proxyServiceName;
	}
	
	/**
	 * The proxy authentication service name.
	 * 
	 * @param string $proxyServiceName
	 */
	public function setProxyServiceName(string $proxyServiceName) : void
	{
		$this->_proxyServiceName = $proxyServiceName;
	}
	
	/**
	 * Gets the proxy user password.
	 * 
	 * @return ?string
	 */
	public function getProxyUserPassword() : ?string
	{
		return $this->_proxyUserPassword;
	}
	
	/**
	 * A username and password formatted as "[username]:[password]" to use for
	 * the connection to the proxy.
	 * 
	 * @param string $proxyUserPassword
	 */
	public function setProxyUserPassword(string $proxyUserPassword) : void
	{
		$this->_proxyUserPassword = $proxyUserPassword;
	}
	
	/**
	 * Gets the random file value.
	 * 
	 * @return ?string
	 */
	public function getRandomFile() : ?string
	{
		return $this->_randomFile;
	}
	
	/**
	 * A filename to be used to seed the random number generator for SSL.
	 * 
	 * @param string $randomFile
	 */
	public function setRandomFile(string $randomFile) : void
	{
		$this->_randomFile = $randomFile;
	}
	
	/**
	 * Gets the range value.
	 * 
	 * @return ?string
	 */
	public function getRange() : ?string
	{
		return $this->_range;
	}
	
	/**
	 * Range(s) of data to retrieve in the format "X-Y" where X or Y are
	 * optional. HTTP transfers also support several intervals, separated with
	 * commas in the format "X-Y,N-M". 
	 * 
	 * @param string $range
	 */
	public function setRange(string $range) : void
	{
		$this->_range = $range;
	}
	
	/**
	 * Gets the referer value.
	 * 
	 * @return ?string
	 */
	public function getReferer() : ?string
	{
		return $this->_referer;
	}
	
	/**
	 * The contents of the "Referer: " header to be used in a HTTP request.
	 * 
	 * @param string $referer
	 */
	public function setReferer(string $referer) : void
	{
		$this->_referer = $referer;
	}
	
	/**
	 * Gets the service name.
	 * 
	 * @return ?string
	 */
	public function getServiceName() : ?string
	{
		return $this->_serviceName;
	}
	
	/**
	 * The authentication service name.
	 * 
	 * @param string $serviceName
	 */
	public function setServiceName(string $serviceName) : void
	{
		$this->_serviceName = $serviceName;
	}
	
	/**
	 * Gets the ssh host public key md5 value.
	 * 
	 * @return ?string
	 */
	public function getSshHostPublicKeyMd5() : ?string
	{
		return $this->_sshHostPublicKeyMd5;
	}
	
	/**
	 * A string containing 32 hexadecimal digits. The string should be the MD5
	 * checksum of the remote host's public key, and libcurl will reject the
	 * connection to the host unless the md5sums match. This option is only for
	 * SCP and SFTP transfers. 
	 * 
	 * @param string $sshHostPublicKeyMd5
	 */
	public function setSshHostPublicKeyMd5(string $sshHostPublicKeyMd5) : void
	{
		$this->_sshHostPublicKeyMd5 = $sshHostPublicKeyMd5;
	}
	
	/**
	 * Gets the ssh public key file value.
	 * 
	 * @return ?string
	 */
	public function getSshPublicKeyFile() : ?string
	{
		return $this->_sshPublicKeyFile;
	}
	
	/**
	 * The file name for your public key. If not used, libcurl defaults to
	 * $HOME/.ssh/id_dsa.pub if the HOME environment variable is set, and just
	 * "id_dsa.pub" in the current directory if HOME is not set.
	 * 
	 * @param string $sshPublicKeyFile
	 */
	public function setSshPublicKeyFile(string $sshPublicKeyFile) : void
	{
		$this->_sshPublicKeyFile = $sshPublicKeyFile;
	}
	
	/**
	 * Gets the ssh private key file.
	 * 
	 * @return ?string
	 */
	public function getSshPrivateKeyFile() : ?string
	{
		return $this->_sshPrivateKeyFile;
	}
	
	/**
	 * The file name for your private key. If not used, libcurl defaults to
	 * $HOME/.ssh/id_dsa if the HOME environment variable is set, and just
	 * "id_dsa" in the current directory if HOME is not set. If the file is
	 * password-protected, set the password with CURLOPT_KEYPASSWD. 
	 * 
	 * @param string $sshPrivateKeyFile
	 */
	public function setSshPrivateKeyFile(string $sshPrivateKeyFile) : void
	{
		$this->_sshPrivateKeyFile = $sshPrivateKeyFile;
	}
	
	/**
	 * Gets the ssl cipher list.
	 * 
	 * @return ?string
	 */
	public function getSslCipherList() : ?string
	{
		return $this->_sslCipherList;
	}
	
	/**
	 * A list of ciphers to use for SSL. For example, RC4-SHA and TLSv1 are
	 * valid cipher lists.
	 * 
	 * @param string $sslCipherList
	 */
	public function setSslCipherList(string $sslCipherList) : void
	{
		$this->_sslCipherList = $sslCipherList;
	}
	
	/**
	 * Gets ssl cert value.
	 * 
	 * @return ?string
	 */
	public function getSslCert() : ?string
	{
		return $this->_sslCert;
	}
	
	/**
	 * The name of a file containing a PEM formatted certificate.
	 * 
	 * @param string $sslCert
	 */
	public function setSslCert(string $sslCert) : void
	{
		$this->_sslCert = $sslCert;
	}
	
	/**
	 * Gets the ssl cert password value.
	 * 
	 * @return ?string
	 */
	public function getSslCertPassword() : ?string
	{
		return $this->_sslCertPassword;
	}
	
	/**
	 * The password required to use the CURLOPT_SSLCERT certificate.
	 * 
	 * @param string $sslCertPassword
	 */
	public function setSslCertPassword(string $sslCertPassword) : void
	{
		$this->_sslCertPassword = $sslCertPassword;
	}
	
	/**
	 * Gets the ssl cert type value.
	 * 
	 * @return ?CurlSslKeyTypeInterface
	 */
	public function getSslCertType() : ?CurlSslKeyTypeInterface
	{
		return $this->_sslCertType;
	}
	
	/**
	 * The format of the certificate. Supported formats are "PEM" (default),
	 * "DER", and "ENG".
	 * 
	 * @param CurlSslKeyTypeInterface $sslCertType
	 */
	public function setSslCertType(CurlSslKeyTypeInterface $sslCertType) : void
	{
		$this->_sslCertType = $sslCertType;
	}
	
	/**
	 * Gets the ssl engine value.
	 * 
	 * @return ?string
	 */
	public function getSslEngine() : ?string
	{
		return $this->_sslEngine;
	}
	
	/**
	 * The identifier for the crypto engine of the private SSL key specified
	 * in CURLOPT_SSLKEY.
	 * 
	 * @param string $sslEngine
	 */
	public function setSslEngine(string $sslEngine) : void
	{
		$this->_sslEngine = $sslEngine;
	}
	
	/**
	 * Gets the ssl engine default value.
	 * 
	 * @return ?string
	 */
	public function getSslEngineDefault() : ?string
	{
		return $this->_sslEngineDefault;
	}
	
	/**
	 * The identifier for the crypto engine used for asymmetric crypto operations.
	 * 
	 * @param string $sslEngineDefault
	 */
	public function setSslEngineDefault(string $sslEngineDefault) : void
	{
		$this->_sslEngineDefault = $sslEngineDefault;
	}
	
	/**
	 * Gets the ssl key value.
	 * 
	 * @return ?string
	 */
	public function getSslKey() : ?string
	{
		return $this->_sslKey;
	}
	
	/**
	 * The name of a file containing a private SSL key.
	 * 
	 * @param string $sslKey
	 */
	public function setSslKey(string $sslKey) : void
	{
		$this->_sslKey = $sslKey;
	}
	
	/**
	 * Gets the ssl key password value.
	 * 
	 * @return ?string
	 */
	public function getSslKeyPassword() : ?string
	{
		return $this->_sslKeyPassword;
	}
	
	/**
	 * The secret password needed to use the private SSL key specified in
	 * CURLOPT_SSLKEY.
	 * 
	 * Note: Since this option contains a sensitive password, remember to keep
	 * the PHP script it is contained within safe. 
	 * 
	 * @param string $sslKeyPassword
	 */
	public function setSslKeyPassword(string $sslKeyPassword) : void
	{
		$this->_sslKeyPassword = $sslKeyPassword;
	}
	
	/**
	 * Gets the ssl key type value.
	 * 
	 * @return ?CurlSslKeyTypeInterface
	 */
	public function getSslKeyType() : ?CurlSslKeyTypeInterface
	{
		return $this->_sslKeyType;
	}
	
	/**
	 * The key type of the private SSL key specified in CURLOPT_SSLKEY.
	 * Supported key types are "PEM" (default), "DER", and "ENG". 
	 * 
	 * @param CurlSslKeyTypeInterface $sslKeyType
	 */
	public function setSslKeyType(CurlSslKeyTypeInterface $sslKeyType) : void
	{
		$this->_sslKeyType = $sslKeyType;
	}
	
	/**
	 * Gets the unix socket path value.
	 * 
	 * @return ?string
	 */
	public function getUnixSocketPath() : ?string
	{
		return $this->_unixSocketPath;
	}
	
	/**
	 * Enables the use of Unix domain sockets as connection endpoint and sets
	 * the path to the given string. 
	 * 
	 * @param string $unixSocketPath
	 */
	public function setUnixSocketPath(string $unixSocketPath) : void
	{
		$this->_unixSocketPath = $unixSocketPath;
	}
	
	/**
	 * Gets the user agent value.
	 * 
	 * @return ?UserAgentInterface
	 */
	public function getUserAgent() : ?UserAgentInterface
	{
		return $this->_userAgent;
	}
	
	/**
	 * The contents of the "User-Agent: " header to be used in a HTTP request.
	 * 
	 * @param UserAgentInterface $userAgent
	 */
	public function setUserAgent(UserAgentInterface $userAgent) : void
	{
		$this->_userAgent = $userAgent;
	}
	
	/**
	 * Gets the username value.
	 * 
	 * @return ?string
	 */
	public function getUsername() : ?string
	{
		return $this->_username;
	}
	
	/**
	 * The user name to use in authentication.
	 * 
	 * @param string $username
	 */
	public function setUsername(string $username) : void
	{
		$this->_username = $username;
	}
	
	/**
	 * Gets the user password value.
	 * 
	 * @return ?string
	 */
	public function getUserPassword() : ?string
	{
		return $this->_userPassword;
	}
	
	/**
	 * A username and password formatted as "[username]:[password]" to use for
	 * the connection. 
	 * 
	 * @param string $userPassword
	 */
	public function setUserPassword(string $userPassword) : void
	{
		$this->_userPassword = $userPassword;
	}
	
	/**
	 * Gets the xo auth h2 bearer token value.
	 * 
	 * @return ?string
	 */
	public function getXoAuthH2Bearer() : ?string
	{
		return $this->_xoAuthH2Bearer;
	}
	
	/**
	 * Specifies the OAuth 2.0 access token.
	 * 
	 * @param string $xoAuthH2Bearer
	 */
	public function setXoAuthH2Bearer(string $xoAuthH2Bearer) : void
	{
		$this->_xoAuthH2Bearer = $xoAuthH2Bearer;
	}
	
	/**
	 * Gets the connect to values.
	 * 
	 * @return array<integer, string>
	 */
	public function getConnectTos() : array
	{
		return $this->_connectTos;
	}
	
	/**
	 * Connect to a specific host and port instead of the URL's host and port.
	 * Accepts an array of strings with the format 
	 * HOST:PORT:CONNECT-TO-HOST:CONNECT-TO-PORT. 
	 * 
	 * @param array<integer|string, string> $connectTos
	 */
	public function setConnectTos(array $connectTos) : void
	{
		$this->_connectTos = \array_values($connectTos);
	}
	
	/**
	 * Gets the http 200 alias values.
	 * 
	 * @return array<integer, integer>
	 */
	public function getHttp200Aliases() : array
	{
		return $this->_http200Aliases;
	}
	
	/**
	 * An array of HTTP 200 responses that will be treated as valid responses
	 * and not as errors.
	 * 
	 * @param array<integer|string, integer> $http200Aliases
	 */
	public function setHttp200Aliases(array $http200Aliases) : void
	{
		$this->_http200Aliases = \array_values($http200Aliases);
	}
	
	/**
	 * Gets the http header values.
	 * 
	 * @return array<integer, string>
	 */
	public function getHttpHeaders() : array
	{
		return $this->_httpHeaders;
	}
	
	/**
	 * An array of HTTP header fields to set, in the format 
	 * ['Content-type: text/plain', 'Content-length: 100'].
	 * 
	 * @param array<integer|string, string> $httpHeaders
	 */
	public function setHttpHeaders(array $httpHeaders) : void
	{
		$this->_httpHeaders = \array_values($httpHeaders);
	}
	
	/**
	 * Gets the proxy header values.
	 * 
	 * @return array<integer, string>
	 */
	public function getProxyHeaders() : array
	{
		return $this->_proxyHeaders;
	}
	
	/**
	 * An array of custom HTTP headers to pass to proxies.
	 * 
	 * @param array<integer|string, string> $proxyHeaders
	 */
	public function setProxyHeaders(array $proxyHeaders) : void
	{
		$this->_proxyHeaders = \array_values($proxyHeaders);
	}
	
	/**
	 * Gets the post quote values.
	 * 
	 * @return array<integer, string>
	 */
	public function getPostQuote() : array
	{
		return $this->_postQuote;
	}
	
	/**
	 * An array of FTP commands to execute on the server after the FTP request
	 * has been performed. 
	 * 
	 * @param array<integer|string, string> $postQuote
	 */
	public function setPostQuote(array $postQuote) : void
	{
		$this->_postQuote = \array_values($postQuote);
	}
	
	/**
	 * Gets the quote values.
	 * 
	 * @return array<integer, string>
	 */
	public function getQuote() : array
	{
		return $this->_quote;
	}
	
	/**
	 * An array of FTP commands to execute on the server prior to the FTP request.
	 * 
	 * @param array<integer|string, string> $quote
	 */
	public function setQuote(array $quote) : void
	{
		$this->_quote = \array_values($quote);
	}
	
	/**
	 * Gets the resolve values.
	 * 
	 * @return array<integer, string>
	 */
	public function getResolve() : array
	{
		return $this->_resolve;
	}
	
	/**
	 * Provide a custom address for a specific host and port pair. An array of
	 * hostname, port, and IP address strings, each element separated by a
	 * colon. In the format: ["example.com:80:127.0.0.1"].
	 * 
	 * @param array<integer|string, string> $resolve
	 */
	public function setResolve(array $resolve) : void
	{
		$this->_resolve = \array_values($resolve);
	}
	
	/**
	 * Gets the stdout resource.
	 * 
	 * @return ?resource
	 */
	public function getFileStdout()
	{
		return $this->_fileStdout;
	}
	
	/**
	 * The file that the transfer should be written to. The default is STDOUT
	 * (the browser window).
	 * 
	 * @param resource $fileStdout
	 */
	public function setFileStdout($fileStdout) : void
	{
		$this->_fileStdout = $fileStdout;
	}
	
	/**
	 * Gets the stdin resource.
	 * 
	 * @return ?resource
	 */
	public function getFileStdin()
	{
		return $this->_fileStdin;
	}
	
	/**
	 * The file that the transfer should be read from when uploading.
	 * 
	 * @param resource $fileStdin
	 */
	public function setFileStdin($fileStdin) : void
	{
		$this->_fileStdin = $fileStdin;
	}
	
	/**
	 * Gets the stderr resource.
	 * 
	 * @return ?resource
	 */
	public function getFileStderr()
	{
		return $this->_fileStderr;
	}
	
	/**
	 * An alternative location to output errors to instead of STDERR.
	 * 
	 * @param resource $fileStderr
	 */
	public function setFileStderr($fileStderr) : void
	{
		$this->_fileStderr = $fileStderr;
	}
	
	/**
	 * Gets the stdout resource for the headers.
	 * 
	 * @return ?resource
	 */
	public function getFileHeader()
	{
		return $this->_fileHeader;
	}
	
	/**
	 * The file that the header part of the transfer is written to.
	 * 
	 * @param resource $fileHeader
	 */
	public function setFileHeader($fileHeader) : void
	{
		$this->_fileHeader = $fileHeader;
	}
	
	/**
	 * Gets the header callback function.
	 * 
	 * @return ?CurlHeaderFunctionInterface
	 */
	public function getHeaderFunction() : ?CurlHeaderFunctionInterface
	{
		return $this->_headerFunction;
	}
	
	/**
	 * A callback accepting two parameters. The first is the cURL resource,
	 * the second is a string with the header data to be written. The header
	 * data must be written by this callback. Return the number of bytes
	 * written. 
	 * 
	 * @param CurlHeaderFunctionInterface $headerFunction
	 */
	public function setHeaderFunction(CurlHeaderFunctionInterface $headerFunction) : void
	{
		$this->_headerFunction = $headerFunction;
	}
	
	/**
	 * Gets the password callback function.
	 * 
	 * @return ?CurlPasswordFunctionInterface
	 */
	public function getPasswordFunction() : ?CurlPasswordFunctionInterface
	{
		return $this->_passwordFunction;
	}
	
	/**
	 * A callback accepting three parameters. The first is the cURL resource,
	 * the second is a string containing a password prompt, and the third is
	 * the maximum password length. Return the string containing the password. 
	 * 
	 * @param CurlPasswordFunctionInterface $passwordFunction
	 */
	public function setPasswordFunction(CurlPasswordFunctionInterface $passwordFunction) : void
	{
		$this->_passwordFunction = $passwordFunction;
	}
	
	/**
	 * Gets the progress callback function.
	 * 
	 * @return ?CurlProgressFunctionInterface
	 */
	public function getProgressFunction() : ?CurlProgressFunctionInterface
	{
		return $this->_progressFunction;
	}
	
	/**
	 * A callback accepting five parameters. The first is the cURL resource,
	 * the second is the total number of bytes expected to be downloaded in
	 * this transfer, the third is the number of bytes downloaded so far, the
	 * fourth is the total number of bytes expected to be uploaded in this
	 * transfer, and the fifth is the number of bytes uploaded so far.
	 * 
	 * Note: The callback is only called when the CURLOPT_NOPROGRESS option is
	 * set to FALSE. Return a non-zero value to abort the transfer. In which
	 * case, the transfer will set a CURLE_ABORTED_BY_CALLBACK error. 
	 * 
	 * @param CurlProgressFunctionInterface $progressFunction
	 */
	public function setProgressFunction(CurlProgressFunctionInterface $progressFunction) : void
	{
		$this->_progressFunction = $progressFunction;
	}
	
	/**
	 * Gets the read callback function.
	 * 
	 * @return ?CurlReadFunctionInterface
	 */
	public function getReadFunction() : ?CurlReadFunctionInterface
	{
		return $this->_readFunction;
	}
	
	/**
	 * A callback accepting three parameters. The first is the cURL resource,
	 * the second is a stream resource provided to cURL through the option
	 * CURLOPT_INFILE, and the third is the maximum amount of data to be read.
	 * The callback must return a string with a length equal or smaller than
	 * the amount of data requested, typically by reading it from the passed
	 * stream resource. It should return an empty string to signal EOF.
	 * 
	 * @param CurlReadFunctionInterface $readFunction
	 */
	public function setReadFunction(CurlReadFunctionInterface $readFunction) : void
	{
		$this->_readFunction = $readFunction;
	}
	
	/**
	 * Gets the write callback function.
	 * 
	 * @return ?CurlWriteFunctionInterface
	 */
	public function getWriteFunction() : ?CurlWriteFunctionInterface
	{
		return $this->_writeFunction;
	}
	
	/**
	 * A callback accepting two parameters. The first is the cURL resource,
	 * and the second is a string with the data to be written. The data must
	 * be saved by this callback. It must return the exact number of bytes
	 * written or the transfer will be aborted with an error. 
	 * 
	 * @param CurlWriteFunctionInterface $writeFunction
	 */
	public function setWriteFunction(CurlWriteFunctionInterface $writeFunction) : void
	{
		$this->_writeFunction = $writeFunction;
	}
	
	/**
	 * Gets the shared handle.
	 * 
	 * @return ?CurlSharedInterface
	 */
	public function getSharedHandle() : ?CurlSharedInterface
	{
		return $this->_sharedHandle;
	}
	
	/**
	 * A result of curl_share_init(). Makes the cURL handle to use the data
	 * from the shared handle. 
	 * 
	 * @param CurlSharedInterface $sharedHandle
	 */
	public function setSharedHandle(CurlSharedInterface $sharedHandle) : void
	{
		$this->_sharedHandle = $sharedHandle;
	}
	
	/**
	 * Creates a new CurlOptions which is the merges of this options and
	 * the other options. In case of conflict and where merging is impossible
	 * or meaningless, the other options values takes precedence.
	 * 
	 * @param ?CurlOptions $other
	 * @return CurlOptions
	 */
	public function mergeWith(?CurlOptions $other) : CurlOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$new = new self();
		
		$new->_autoreferer = $this->_autoreferer || $other->_autoreferer;
		$new->_cookieSession = $this->_cookieSession || $other->_cookieSession;
		$new->_certInfo = $this->_certInfo || $other->_certInfo;
		$new->_connectOnly = $this->_connectOnly || $other->_connectOnly;
		$new->_dnsUseGlobalCache = $this->_dnsUseGlobalCache || $other->_dnsUseGlobalCache;
		$new->_sslFalseStart = $this->_sslFalseStart || $other->_sslFalseStart;
		$new->_fileTime = $this->_fileTime || $other->_fileTime;
		$new->_followLocation = $this->_followLocation || $other->_followLocation;
		$new->_forbidReuse = $this->_forbidReuse || $other->_forbidReuse;
		$new->_freshConnect = $this->_freshConnect || $other->_freshConnect;
		$new->_ftpUseEprt = $this->_ftpUseEprt || $other->_ftpUseEprt;
		$new->_ftpUseEpsv = $this->_ftpUseEpsv || $other->_ftpUseEpsv;
		$new->_ftpCreateMissingDirs = $this->_ftpCreateMissingDirs || $other->_ftpCreateMissingDirs;
		$new->_ftpAppend = $this->_ftpAppend || $other->_ftpAppend;
		$new->_tcpNoDelay = $this->_tcpNoDelay || $other->_tcpNoDelay;
		$new->_ftpListOnly = $this->_ftpListOnly || $other->_ftpListOnly;
		$new->_httpProxyTunnel = $this->_httpProxyTunnel || $other->_httpProxyTunnel;
		$new->_netrc = $this->_netrc || $other->_netrc;
		$new->_noProgress = $this->_noProgress || $other->_noProgress;
		$new->_pathAsIs = $this->_pathAsIs || $other->_pathAsIs;
		$new->_pipeWait = $this->_pipeWait || $other->_pipeWait;
		$new->_saslIr = $this->_saslIr || $other->_saslIr;
		$new->_sslAlpn = $this->_sslAlpn || $other->_sslAlpn;
		$new->_sslNpn = $this->_sslNpn || $other->_sslNpn;
		$new->_sslVerifyPeer = $this->_sslVerifyPeer || $other->_sslVerifyPeer;
		$new->_sslVerifyStatus = $this->_sslVerifyStatus || $other->_sslVerifyStatus;
		$new->_tcpFastOpen = $this->_tcpFastOpen || $other->_tcpFastOpen;
		$new->_tftpNoOptions = $this->_tftpNoOptions || $other->_tftpNoOptions;
		$new->_transferText = $this->_transferText || $other->_transferText;
		$new->_unrestrictedAuth = $this->_unrestrictedAuth || $other->_unrestrictedAuth;
		$new->_verbose = $this->_verbose || $other->_verbose;
		
		$new->_bufferSize = \max($this->_bufferSize, $other->_bufferSize);
		$new->_connectionTimeout = \max($this->_connectionTimeout, $other->_connectionTimeout);
		$new->_connectionTimeoutMs = \max($this->_connectionTimeoutMs, $other->_connectionTimeoutMs);
		$new->_dnsCacheTimeout = \max($this->_dnsCacheTimeout, $other->_dnsCacheTimeout);
		$new->_expect100TimeoutMs = \max($this->_expect100TimeoutMs, $other->_expect100TimeoutMs);
		$new->_ftpSslAuth = $other->getFtpSslAuth() ?? $this->getFtpSslAuth();
		$new->_headerOpt = $other->getHeaderOpt() ?? $this->getHeaderOpt();
		$new->_httpVersion = $other->getHttpVersion() ?? $this->getHttpVersion();
		
		
		if(null !== $other->_httpAuth && null !== $this->_httpAuth)
		{
			$new->_httpAuth = $this->_httpAuth->with($other->_httpAuth);
		}
		else
		{
			$new->_httpAuth = $other->_httpAuth ?? $this->_httpAuth;
		}
		
		$new->_lowSpeedLimit = \max($this->_lowSpeedLimit, $other->_lowSpeedLimit);
		$new->_lowSpeedTime = \max($this->_lowSpeedTime, $other->_lowSpeedTime);
		$new->_maxConnects = \max($this->_maxConnects, $other->_maxConnects);
		$new->_maxRedirs = \max($this->_maxRedirs, $other->_maxRedirs);
		$new->_port = $other->getPort() ?? $this->getPort();
		$new->_postRedir = \max($this->_postRedir, $other->_postRedir);
		
		if(null !== $other->_protocol && null !== $this->_protocol)
		{
			$new->_protocol = $this->_protocol->with($other->_protocol);
		}
		else
		{
			$new->_protocol = $other->_protocol ?? $this->_protocol;
		}
		
		if(null !== $other->_proxyAuth && null !== $this->_proxyAuth)
		{
			$new->_proxyAuth = $this->_proxyAuth->with($other->_proxyAuth);
		}
		else
		{
			$new->_proxyAuth = $other->_proxyAuth ?? $this->_proxyAuth;
		}
		
		$new->_proxyPort = $other->getProxyPort() ?? $this->getProxyPort();
		$new->_proxyType = $other->getProxyType() ?? $this->getProxyType();
		
		if(null !== $other->_redirProtocol && null !== $this->_redirProtocol)
		{
			$new->_redirProtocol = $this->_redirProtocol->with($other->_redirProtocol);
		}
		else
		{
			$new->_redirProtocol = $other->_redirProtocol ?? $this->_redirProtocol;
		}
		
		$new->_resumeFrom = $other->getResumeFrom() ?? $this->getResumeFrom();
		$new->_sslOptions = $other->getSslOptions() ?? $this->getSslOptions();
		$new->_sslVerifyHost = $other->getSslVerifyHost() ?? $this->getSslVerifyHost();
		$new->_sslVersion = $other->getSslVersion() ?? $this->getSslVersion();
		$new->_streamWeight = $other->getStreamWeight() ?? $this->getStreamWeight();
		$new->_timeCondition = $other->getTimeCondition() ?? $this->getTimeCondition();
		$new->_timeout = \max($this->getTimeout(), $other->getTimeout());
		$new->_timeoutMs = \max($this->getTimeoutMs(), $other->getTimeoutMs());
		$new->_timeValue = $other->getTimeValue() ?? $this->getTimeValue();
		$new->_maxRecvSpeedLarge = \max($this->_maxRecvSpeedLarge, $other->_maxRecvSpeedLarge);
		$new->_maxSendSpeedLarge = \max($this->_maxSendSpeedLarge, $other->_maxSendSpeedLarge);
		
		if(null !== $other->_sslAuthType && null !== $this->_sslAuthType)
		{
			$new->_sslAuthType = $this->_sslAuthType->with($other->_sslAuthType);
		}
		else
		{
			$new->_sslAuthType = $other->_sslAuthType ?? $this->_sslAuthType;
		}
		
		$new->_ipResolve = $other->getIpResolve() ?? $this->getIpResolve();
		$new->_ftpFileMethod = $other->getFtpFileMethod() ?? $this->getFtpFileMethod();
		$new->_caInfo = $other->getCaInfo() ?? $this->getCaInfo();
		$new->_caPath = $other->getCaPath() ?? $this->getCaPath();
		$new->_cookie = \array_merge($this->getCookie(), $other->getCookie());
		$new->_cookieFile = $other->getCookieFile() ?? $this->getCookieFile();
		$new->_cookieJarFile = $other->getCookieJarFile() ?? $this->getCookieJarFile();
		$new->_customRequest = $other->getCustomRequest() ?? $this->getCustomRequest();
		$new->_defaultProtocol = $other->getDefaultProtocol() ?? $this->getDefaultProtocol();
		$new->_dnsInterface = $other->getDnsInterface() ?? $this->getDnsInterface();
		$new->_dnsLocalIp4 = $other->getDnsLocalIp4() ?? $this->getDnsLocalIp4();
		$new->_dnsLocalIp6 = $other->getDnsLocalIp6() ?? $this->getDnsLocalIp6();
		$new->_egdSocket = $other->getEgdSocket() ?? $this->getEgdSocket();
		$new->_ftpPort = $other->getFtpPort() ?? $this->getFtpPort();
		$new->_interface = $other->getInterface() ?? $this->getInterface();
		$new->_keyPassword = $other->getKeyPassword() ?? $this->getKeyPassword();
		$new->_krb4Level = $other->getKrb4Level() ?? $this->getKrb4Level();
		$new->_loginOptions = $other->getLoginOptions() ?? $this->getLoginOptions();
		$new->_pinnedPublicKey = $other->getPinnedPublicKey() ?? $this->getPinnedPublicKey();
		$new->_postFields = \trim(((string) $this->_postFields).'&'.((string) $other->_postFields), '&');
		$new->_private = \array_merge($this->_private, $other->_private);
		$new->_proxy = $other->getProxy() ?? $this->getProxy();
		$new->_proxyServiceName = $other->getProxyServiceName() ?? $this->getProxyServiceName();
		$new->_proxyUserPassword = $other->getProxyUserPassword() ?? $this->getProxyUserPassword();
		$new->_randomFile = $this->getRandomFile() ?? $this->getRandomFile();
		$new->_range = $other->getRange() ?? $this->getRange();
		$new->_referer = $other->getReferer() ?? $this->getReferer();
		$new->_serviceName = $other->getServiceName() ?? $this->getServiceName();
		$new->_sshHostPublicKeyMd5 = $other->getSshHostPublicKeyMd5() ?? $this->getSshHostPublicKeyMd5();
		$new->_sshPublicKeyFile = $other->getSshPublicKeyFile() ?? $this->getSshPublicKeyFile();
		$new->_sshPrivateKeyFile = $other->getSshPrivateKeyFile() ?? $this->getSshPrivateKeyFile();
		$new->_sslCipherList = \trim(((string) $this->_sslCipherList).','.((string) $other->_sslCipherList), ',');
		$new->_sslCert = $other->getSslCert() ?? $this->getSslCert();
		$new->_sslCertPassword = $other->getSslCertPassword() ?? $this->getSslCertPassword();
		$new->_sslCertType = $other->getSslCertType() ?? $this->getSslCertType();
		$new->_sslEngine = $other->getSslEngine() ?? $this->getSslEngine();
		$new->_sslEngineDefault = $other->getSslEngineDefault() ?? $this->getSslEngineDefault();
		$new->_sslKey = $other->getSslKey() ?? $this->getSslKey();
		$new->_sslKeyPassword = $other->getSslKeyPassword() ?? $this->getSslKeyPassword();
		$new->_sslKeyType = $other->getSslKeyType() ?? $this->getSslKeyType();
		$new->_unixSocketPath = $other->getUnixSocketPath() ?? $this->getUnixSocketPath();
		$new->_userAgent = $other->getUserAgent() ?? $this->getUserAgent();
		$new->_username = $other->getUsername() ?? $this->getUsername();
		$new->_userPassword = $other->getUserPassword() ?? $this->getUserPassword();
		$new->_xoAuthH2Bearer = $other->getXoAuthH2Bearer() ?? $this->getXoAuthH2Bearer();
		$new->_connectTos = \array_merge($this->_connectTos, $other->_connectTos);
		$new->_http200Aliases = \array_merge($this->_http200Aliases, $other->_http200Aliases);
		$new->_httpHeaders = \array_merge($this->_httpHeaders, $other->_httpHeaders);
		$new->_proxyHeaders = \array_merge($this->_proxyHeaders, $other->_proxyHeaders);
		$new->_postQuote = \array_merge($this->_postQuote, $other->_postQuote);
		$new->_quote = \array_merge($this->_quote, $other->_quote);
		$new->_resolve = \array_merge($this->_resolve, $other->_resolve);
		$new->_fileStdout = $other->getFileStdout() ?? $this->getFileStdout();
		$new->_fileStdin = $other->getFileStdin() ?? $this->getFileStdin();
		$new->_fileStderr = $other->getFileStderr() ?? $this->getFileStderr();
		$new->_fileHeader = $other->getFileHeader() ?? $this->getFileHeader();
		
		if(null !== $other->_headerFunction && null !== $this->_headerFunction)
		{
			$new->_headerFunction = new CurlCompositeHeaderFunction($other->_headerFunction, $this->_headerFunction);
		}
		else
		{
			$new->_headerFunction = $other->getHeaderFunction() ?? $this->getHeaderFunction();
		}
		
		if(null !== $other->_passwordFunction && null !== $this->_passwordFunction)
		{
			$new->_passwordFunction = new CurlCompositePasswordFunction($other->_passwordFunction, $this->_passwordFunction);
		}
		else
		{
			$new->_passwordFunction = $other->getPasswordFunction() ?? $this->getPasswordFunction();
		}
		
		if(null !== $other->_progressFunction && null !== $this->_progressFunction)
		{
			$new->_progressFunction = new CurlCompositeProgressFunction($other->_progressFunction, $this->_progressFunction);
		}
		else
		{
			$new->_progressFunction = $other->getProgressFunction() ?? $this->getProgressFunction();
		}
		
		if(null !== $other->_readFunction && null !== $this->_readFunction)
		{
			$new->_readFunction = new CurlCompositeReadFunction($this->_readFunction, $other->_readFunction);
		}
		else
		{
			$new->_readFunction = $other->getReadFunction() ?? $this->getReadFunction();
		}
		
		if(null !== $other->_writeFunction && null !== $this->_writeFunction)
		{
			$new->_writeFunction = new CurlCompositeWriteFunction($other->_writeFunction, $this->_writeFunction);
		}
		else
		{
			$new->_writeFunction = $other->getWriteFunction() ?? $this->getWriteFunction();
		}
		
		$new->_sharedHandle = $other->getSharedHandle() ?? $this->getSharedHandle();
		
		return $new;
	}
	
	/**
	 * Applies the options to the curl object, effectively configuring it to 
	 * do the requests.
	 * 
	 * @param CurlInterface $curl
	 * @return array<integer, Exception> $errors
	 */
	public function applyTo(CurlInterface $curl) : array
	{
		$errors = [];
		
		// {{{ parameters of curl taken over by this library
		
		if(!$curl->enableCrlf())
		{
			$errors[] = new Exception('Failed to enable crlf : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->disableFailOnError())
		{
			$errors[] = new Exception('Failed to disable fail on error : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->disableHeader())
		{
			$errors[] = new Exception('Failed to disable header : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->enableHeaderOut())
		{
			$errors[] = new Exception('Failed to disable header out : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->disableNoBody())
		{
			$errors[] = new Exception('Failed to disable no body : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->enableNoSignal())
		{
			$errors[] = new Exception('Failed to enable no signal : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		// }}}
		
		if($this->_autoreferer)
		{
			if(!$curl->enableAutoreferer())
			{
				$errors[] = new Exception('Failed to enable autoreferer : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableAutoreferer())
			{
				$errors[] = new Exception('Failed to disable autoreferer : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_cookieSession)
		{
			if(!$curl->enableCookieSession())
			{
				$errors[] = new Exception('Failed to enable cookie session : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableCookieSession())
			{
				$errors[] = new Exception('Failed to disable cookie session : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_certInfo)
		{
			if(!$curl->enableCertInfo())
			{
				$errors[] = new Exception('Failed to enable cert info : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableCertInfo())
			{
				$errors[] = new Exception('Failed to disable cert info : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_connectOnly)
		{
			if(!$curl->enableConnectOnly())
			{
				$errors[] = new Exception('Failed to enable connect only : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableConnectOnly())
			{
				$errors[] = new Exception('Failed to disable connect only : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_dnsUseGlobalCache)
		{
			if(!$curl->enableDnsUseGlobalCache())
			{
				$errors[] = new Exception('Failed to enable dns use global cache : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableDnsUseGlobalCache())
			{
				$errors[] = new Exception('Failed to disable dns use global cache : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_sslFalseStart)
		{
			if(!$curl->enableSslFalseStart())
			{
				$errors[] = new Exception('Failed to enable ssl false start : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableSslFalseStart())
			{
				$errors[] = new Exception('Failed to disable ssl false start : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_fileTime)
		{
			if(!$curl->enableFiletime())
			{
				$errors[] = new Exception('Failed to enable filetime : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFiletime())
			{
				$errors[] = new Exception('Failed to enable filetime : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_followLocation)
		{
			if(!$curl->enableFollowLocation())
			{
				$errors[] = new Exception('Failed to enable follow location : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFollowLocation())
			{
				$errors[] = new Exception('Failed to disable follow location : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_forbidReuse)
		{
			if(!$curl->enableForbidReuse())
			{
				$errors[] = new Exception('Failed to enable forbid reuse : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableForbidReuse())
			{
				$errors[] = new Exception('Failed to disable forbid reuse : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_freshConnect)
		{
			if(!$curl->enableFreshConnect())
			{
				$errors[] = new Exception('Failed to enable fresh connect : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFreshConnect())
			{
				$errors[] = new Exception('Failed to disable fresh connect : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_ftpUseEprt)
		{
			if(!$curl->enableFtpUseEprt())
			{
				$errors[] = new Exception('Failed to enable ftp use eprt : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFtpUseEpsv())
			{
				$errors[] = new Exception('Failed to disable ftp use eprt : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_ftpUseEpsv)
		{
			if(!$curl->enableFtpUseEpsv())
			{
				$errors[] = new Exception('Failed to enable ftp use epsv : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFtpUseEpsv())
			{
				$errors[] = new Exception('Failed to disable ftp use epsv : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_ftpCreateMissingDirs)
		{
			if(!$curl->enableFtpCreateMissingDirs())
			{
				$errors[] = new Exception('Failed to enable ftp create missing dirs : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFtpCreateMissingDirs())
			{
				$errors[] = new Exception('Failed to disable ftp create missing dirs : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_ftpAppend)
		{
			if(!$curl->enableFtpAppend())
			{
				$errors[] = new Exception('Failed to enable ftp append : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFtpAppend())
			{
				$errors[] = new Exception('Failed to disable ftp append : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_tcpNoDelay)
		{
			if(!$curl->enableTcpNodelay())
			{
				$errors[] = new Exception('Failed to enable tcp no delay : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableTcpNodelay())
			{
				$errors[] = new Exception('Failed to enable tcp no delay : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_ftpListOnly)
		{
			if(!$curl->enableFtpListOnly())
			{
				$errors[] = new Exception('Failed to enable ftp list only : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableFtpListOnly())
			{
				$errors[] = new Exception('Failed to disable ftp list only : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_httpProxyTunnel)
		{
			if(!$curl->enableHttpProxyTunnel())
			{
				$errors[] = new Exception('Failed to enable http proxy tunnel : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableHttpProxyTunnel())
			{
				$errors[] = new Exception('Failed to disable http proxy tunnel : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_netrc)
		{
			if(!$curl->enableNetrc())
			{
				$errors[] = new Exception('Failed to enable netrc : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableNetrc())
			{
				$errors[] = new Exception('Failed to disable netrc : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_noProgress)
		{
			if(!$curl->enableNoProgress())
			{
				$errors[] = new Exception('Failed to enable no progress : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableNoProgress())
			{
				$errors[] = new Exception('Failed to disable no progress : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_pathAsIs)
		{
			if(!$curl->enablePathAsIs())
			{
				$errors[] = new Exception('Failed to enable path as is : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disablePathAsIs())
			{
				$errors[] = new Exception('Failed to disable path as is : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_pipeWait)
		{
			if(!$curl->enablePipeWait())
			{
				$errors[] = new Exception('Failed to enable pipe wait : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disablePipeWait())
			{
				$errors[] = new Exception('Failed to disable pipe wait : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_saslIr)
		{
			if(!$curl->enableSaslIr())
			{
				$errors[] = new Exception('Failed to enable sasl ir : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableSaslIr())
			{
				$errors[] = new Exception('Failed to disable sasl ir : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_sslAlpn)
		{
			if(!$curl->enableSslAlpn())
			{
				$errors[] = new Exception('Failed to enable ssl alpn : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableSslAlpn())
			{
				$errors[] = new Exception('Failed to disable ssl alpn : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_sslNpn)
		{
			if(!$curl->enableSslNpn())
			{
				$errors[] = new Exception('Failed to enable ssl npn : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableSslNpn())
			{
				$errors[] = new Exception('Failed to disable ssl npn : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_sslVerifyPeer)
		{
			if(!$curl->enableSslVerifyPeer())
			{
				$errors[] = new Exception('Failed to enable ssl verify peer : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableSslVerifyPeer())
			{
				$errors[] = new Exception('Failed to disable ssl verify peer : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_sslVerifyStatus)
		{
			if(!$curl->enableSslVerifyStatus())
			{
				$errors[] = new Exception('Failed to enable ssl verify status : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableSslVerifyStatus())
			{
				$errors[] = new Exception('Failed to disable ssl verify status : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_tcpFastOpen)
		{
			if(!$curl->enableTcpFastOpen())
			{
				$errors[] = new Exception('Failed to enable tcp fast open : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableTcpFastOpen())
			{
				$errors[] = new Exception('Failed to disable tcp fast open : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_tftpNoOptions)
		{
			if(!$curl->enableTftpNoOptions())
			{
				$errors[] = new Exception('Failed to enable tftp no options : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableTftpNoOptions())
			{
				$errors[] = new Exception('Failed to disable tftp no options : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_transferText)
		{
			if(!$curl->enableTransferText())
			{
				$errors[] = new Exception('Failed to enable transfer text : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableTransferText())
			{
				$errors[] = new Exception('Failed to disable transfer text : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_unrestrictedAuth)
		{
			if(!$curl->enableUnrestrictedAuth())
			{
				$errors[] = new Exception('Failed to enable unrestricted auth : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableUnrestrictedAuth())
			{
				$errors[] = new Exception('Failed to disable unrestricted auth : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if($this->_verbose)
		{
			if(!$curl->enableVerbose())
			{
				$errors[] = new Exception('Failed to enable verbose : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		else
		{
			if(!$curl->disableVerbose())
			{
				$errors[] = new Exception('Failed to disable verbose : '.$curl->getErrorMessage(), $curl->getErrorCode());
			}
		}
		
		if(!$curl->setBufferSize($this->_bufferSize))
		{
			$errors[] = new Exception('Failed to set buffer size : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setConnectionTimeout($this->_connectionTimeout))
		{
			$errors[] = new Exception('Failed to set connection timeout : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setConnectionTimeoutMs($this->_connectionTimeoutMs))
		{
			$errors[] = new Exception('Failed to set connection timeout ms : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setDnsCacheTimeout($this->_dnsCacheTimeout))
		{
			$errors[] = new Exception('Failed to set dns cache timeout : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setExpect100TimeoutMs($this->_expect100TimeoutMs))
		{
			$errors[] = new Exception('Failed to set expect 100 timeout ms : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_ftpSslAuth && !$curl->setFtpSslAuth($this->_ftpSslAuth))
		{
			$errors[] = new Exception('Failed to set connection timeout ms : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_headerOpt && !$curl->setHeaderOpt($this->_headerOpt))
		{
			$errors[] = new Exception('Failed to set header opt : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_httpAuth && !$curl->setHttpAuth($this->_httpAuth))
		{
			$errors[] = new Exception('Failed to set http auth : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setLowSpeedLimit($this->_lowSpeedLimit))
		{
			$errors[] = new Exception('Failed to set low speed limit : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setLowSpeedTime($this->_lowSpeedTime))
		{
			$errors[] = new Exception('Failed to set low speed time : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setMaxConnects($this->_maxConnects))
		{
			$errors[] = new Exception('Failed to set max connects : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setMaxRedirs($this->_maxRedirs))
		{
			$errors[] = new Exception('Failed to set max redirs : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_port && !$curl->setPort($this->_port))
		{
			$errors[] = new Exception('Failed to set port : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_postRedir && !$curl->setPostRedir($this->_postRedir))
		{
			$errors[] = new Exception('Failed to set post redir : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_protocol && !$curl->setProtocols($this->_protocol))
		{
			$errors[] = new Exception('Failed to set protocol : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_proxyAuth && !$curl->setProxyAuth($this->_proxyAuth))
		{
			$errors[] = new Exception('Failed to set proxy auth : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_proxyPort && !$curl->setProxyPort($this->_proxyPort))
		{
			$errors[] = new Exception('Failed to set proxy port : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_proxyType && !$curl->setProxyType($this->_proxyType))
		{
			$errors[] = new Exception('Failed to set proxy type : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_redirProtocol && !$curl->setRedirProtocols($this->_redirProtocol))
		{
			$errors[] = new Exception('Failed to set redir protocol : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_resumeFrom && !$curl->setResumeFrom($this->_resumeFrom))
		{
			$errors[] = new Exception('Failed to set resume from : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslOptions && !$curl->setSslOptions($this->_sslOptions))
		{
			$errors[] = new Exception('Failed to set ssl options : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslVerifyHost && !$curl->setSslVerifyHost($this->_sslVerifyHost))
		{
			$errors[] = new Exception('Failed to set ssl verify host : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslVersion && !$curl->setSslVersion($this->_sslVersion))
		{
			$errors[] = new Exception('Failed to set ssl version : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_streamWeight && !$curl->setStreamWeight($this->_streamWeight))
		{
			$errors[] = new Exception('Failed to set stream weight : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_timeCondition && !$curl->setTimeCondition($this->_timeCondition))
		{
			$errors[] = new Exception('Failed to set time condition : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setTimeout($this->_timeout))
		{
			$errors[] = new Exception('Failed to set timeout : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setTimeoutMs($this->_timeoutMs))
		{
			$errors[] = new Exception('Failed to set timeout ms : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_timeValue && !$curl->setTimeValue($this->_timeValue))
		{
			$errors[] = new Exception('Failed to set time value : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setMaxRecvSpeedLarge($this->_maxRecvSpeedLarge))
		{
			$errors[] = new Exception('Failed to set max recv speed large : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(!$curl->setMaxSendSpeedLarge($this->_maxSendSpeedLarge))
		{
			$errors[] = new Exception('Failed to set max send speed large : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslAuthType && !$curl->setSshAuthTypes($this->_sslAuthType))
		{
			$errors[] = new Exception('Failed to set ssl auth type : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_ipResolve && !$curl->setIpResolve($this->_ipResolve))
		{
			$errors[] = new Exception('Failed to set ip resolve : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_ftpFileMethod && !$curl->setFtpFileMethod($this->_ftpFileMethod))
		{
			$errors[] = new Exception('Failed to set ftp file method : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_caInfo && !$curl->setCaInfo($this->_caInfo))
		{
			$errors[] = new Exception('Failed to set ca info : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_caPath && !$curl->setCaPath($this->_caPath))
		{
			$errors[] = new Exception('Failed to set ca path : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_cookie) > 0 && !$curl->setCookie(\implode('; ', $this->_cookie)))
		{
			$errors[] = new Exception('Failed to set cookie : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_cookieFile && !$curl->setCookieFile($this->_cookieFile))
		{
			$errors[] = new Exception('Failed to set cookie file : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_cookieJarFile && !$curl->setCookieJarFile($this->_cookieJarFile))
		{
			$errors[] = new Exception('Failed to set cookie jar file : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_customRequest && !$curl->setCustomRequest($this->_customRequest))
		{
			$errors[] = new Exception('Failed to set custom_request : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_defaultProtocol && !$curl->setDefaultProtocol($this->_defaultProtocol))
		{
			$errors[] = new Exception('Failed to set default protocol : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_dnsInterface && !$curl->setDnsInterface($this->_dnsInterface))
		{
			$errors[] = new Exception('Failed to set dns interface : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_dnsLocalIp4 && !$curl->setDnsLocalIp4($this->_dnsLocalIp4))
		{
			$errors[] = new Exception('Failed to set dns local ipv4 : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_dnsLocalIp6 && !$curl->setDnsLocalIp6($this->_dnsLocalIp6))
		{
			$errors[] = new Exception('Failed to set dns local ipv6 : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_egdSocket && !$curl->setEgdSocket($this->_egdSocket))
		{
			$errors[] = new Exception('Failed to set egd socket : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_encoding && !$curl->setEncoding($this->_encoding))
		{
			$errors[] = new Exception('Failed to set encoding : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_ftpPort && !$curl->setFtpPort($this->_ftpPort))
		{
			$errors[] = new Exception('Failed to set ftp port : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_interface && !$curl->setInterface($this->_interface))
		{
			$errors[] = new Exception('Failed to set interface : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_keyPassword && !$curl->setKeyPassword($this->_keyPassword))
		{
			$errors[] = new Exception('Failed to set key password : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_krb4Level && !$curl->setKrb4Level($this->_krb4Level))
		{
			$errors[] = new Exception('Failed to set krb 4 level : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_loginOptions && !$curl->setLoginOptions($this->_loginOptions))
		{
			$errors[] = new Exception('Failed to set login options : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_pinnedPublicKey && !$curl->setPinnedPublicKey($this->_pinnedPublicKey))
		{
			$errors[] = new Exception('Failed to set pinned public key : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_postFields && !$curl->setPostFields($this->_postFields))
		{
			$errors[] = new Exception('Failed to set post fields : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_private) > 0 && !$curl->setPrivate(\implode('|', $this->_private)))
		{
			$errors[] = new Exception('Failed to set private : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_proxy && !$curl->setProxy($this->_proxy))
		{
			$errors[] = new Exception('Failed to set proxy : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_proxyServiceName && !$curl->setProxyServiceName($this->_proxyServiceName))
		{
			$errors[] = new Exception('Failed to set proxy service name : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_proxyUserPassword && !$curl->setProxyUserPassword($this->_proxyUserPassword))
		{
			$errors[] = new Exception('Failed to set user password : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_randomFile && !$curl->setRandomFile($this->_randomFile))
		{
			$errors[] = new Exception('Failed to set random file : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_range && !$curl->setRange($this->_range))
		{
			$errors[] = new Exception('Failed to set range : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_referer && !$curl->setReferer($this->_referer))
		{
			$errors[] = new Exception('Failed to set referer : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_serviceName && !$curl->setServiceName($this->_serviceName))
		{
			$errors[] = new Exception('Failed to set service name : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sshHostPublicKeyMd5 && !$curl->setSshHostPublicKeyMd5($this->_sshHostPublicKeyMd5))
		{
			$errors[] = new Exception('Failed to set ssh host public key md5 : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sshPublicKeyFile && !$curl->setSshPublicKeyfile($this->_sshPublicKeyFile))
		{
			$errors[] = new Exception('Failed to set ssh public key file : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sshPrivateKeyFile && !$curl->setSshPrivateKeyfile($this->_sshPrivateKeyFile))
		{
			$errors[] = new Exception('Failed to set ssh private key file : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslCipherList && !$curl->setSslCipherList($this->_sslCipherList))
		{
			$errors[] = new Exception('Failed to set ssl cipher list : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslCert && !$curl->setSslCert($this->_sslCert))
		{
			$errors[] = new Exception('Failed to set ssl cert : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslCertPassword && !$curl->setSslCertPasswd($this->_sslCertPassword))
		{
			$errors[] = new Exception('Failed to set ssl cert password : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslCertType && !$curl->setSslCertType($this->_sslCertType))
		{
			$errors[] = new Exception('Failed to set ssl cert type : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslEngine && !$curl->setSslEngine($this->_sslEngine))
		{
			$errors[] = new Exception('Failed to set ssl engine : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslEngineDefault && !$curl->setSslEngineDefault($this->_sslEngineDefault))
		{
			$errors[] = new Exception('Failed to set ssl engine default : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslKey && !$curl->setSslKey($this->_sslKey))
		{
			$errors[] = new Exception('Failed to set ssl key : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslKeyPassword && !$curl->setSslKeyPassword($this->_sslKeyPassword))
		{
			$errors[] = new Exception('Failed to set ssl key password : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sslKeyType && !$curl->setSslKeyType($this->_sslKeyType))
		{
			$errors[] = new Exception('Failed to set ssl key type : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_unixSocketPath && !$curl->setUnixSocketPath($this->_unixSocketPath))
		{
			$errors[] = new Exception('Failed to set unix socket path : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_userAgent && !$curl->setUserAgent($this->_userAgent))
		{
			$errors[] = new Exception('Failed to set user agent : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_username && !$curl->setUsername($this->_username))
		{
			$errors[] = new Exception('Failed to set username : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_userPassword && !$curl->setUserPassword($this->_userPassword))
		{
			$errors[] = new Exception('Failed to set user password : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_xoAuthH2Bearer && !$curl->setXOAuthH2Bearer($this->_xoAuthH2Bearer))
		{
			$errors[] = new Exception('Failed to set XOAuthH2 token : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_connectTos) > 0 && !$curl->setConnectTo($this->_connectTos))
		{
			$errors[] = new Exception('Failed to set connect to : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_http200Aliases) > 0 && !$curl->setHttp200Aliases($this->_http200Aliases))
		{
			$errors[] = new Exception('Failed to set http 200 aliases : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_httpHeaders) > 0 && !$curl->setHttpHeader($this->_httpHeaders))
		{
			$errors[] = new Exception('Failed to set http headers : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_postQuote) > 0 && !$curl->setPostQuote($this->_postQuote))
		{
			$errors[] = new Exception('Failed to set post quote : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_proxyHeaders) > 0 && !$curl->setProxyHeaders($this->_proxyHeaders))
		{
			$errors[] = new Exception('Failed to set proxy headers : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_quote) > 0 && !$curl->setQuote($this->_quote))
		{
			$errors[] = new Exception('Failed to set quote : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(\count($this->_resolve) > 0 && !$curl->setResolve($this->_resolve))
		{
			$errors[] = new Exception('Failed to set resolve : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_fileStdout && !$curl->setFile($this->_fileStdout))
		{
			$errors[] = new Exception('Failed to set file stdout : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_fileStdin && !$curl->setInFile($this->_fileStdin))
		{
			$errors[] = new Exception('Failed to set file stdin : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_fileStderr && !$curl->setStdErr($this->_fileStderr))
		{
			$errors[] = new Exception('Failed to set file stderr : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_fileHeader && !$curl->setWriteHeader($this->_fileHeader))
		{
			$errors[] = new Exception('Failed to set file header : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_headerFunction && !$curl->setHeaderFunction($this->_headerFunction))
		{
			$errors[] = new Exception('Failed to set header function : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_passwordFunction && !$curl->setPasswordFunction($this->_passwordFunction))
		{
			$errors[] = new Exception('Failed to set passwd function : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_progressFunction && !$curl->setProgressFunction($this->_progressFunction))
		{
			$errors[] = new Exception('Failed to set progress function : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_readFunction && !$curl->setReadFunction($this->_readFunction))
		{
			$errors[] = new Exception('Failed to set read function : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_writeFunction && !$curl->setWriteFunction($this->_writeFunction))
		{
			$errors[] = new Exception('Failed to set write function : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		if(null !== $this->_sharedHandle && !$curl->setSharedHandle($this->_sharedHandle))
		{
			$errors[] = new Exception('Failed to set shared handle : '.$curl->getErrorMessage(), $curl->getErrorCode());
		}
		
		// $inFileSize ?
		
		return $errors;
	}
	
}
