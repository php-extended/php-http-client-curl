<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-curl library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\Certificate\CertificateProviderInterface;
use PhpExtended\Curl\CurlHttpVersion;
use PhpExtended\Curl\CurlProtocol;
use PhpExtended\Curl\CurlSslVerifyHostMethod;
use Psr\Http\Message\RequestInterface;
use RuntimeException;
use Stringable;
use TypeError;
use ValueError;

/**
 * CurlOptionsFactory class file.
 * 
 * This class is to create the curl options from the request parameters.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
 * @SuppressWarnings("PHPMD.StaticAccess")
 */
class CurlOptionsFactory implements Stringable
{
	
	/**
	 * The provider for the ssl certs.
	 * 
	 * @var CertificateProviderInterface
	 */
	protected CertificateProviderInterface $_provider;
	
	/**
	 * Builds a new factory with its dependancy.
	 * 
	 * @param CertificateProviderInterface $provider
	 */
	public function __construct(CertificateProviderInterface $provider)
	{
		$this->_provider = $provider;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Creates a new HttpCurlOptions object from the given request.
	 * 
	 * @param RequestInterface $request
	 * @return CurlOptions
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function createFromRequest(RequestInterface $request) : CurlOptions
	{
		$options = new CurlOptions();
		
		// {{{ ssl options, security by default
		
		$options->setSslVerifyHost(CurlSslVerifyHostMethod::HOSTNAME_MATCHES);
		$options->setSslVerifyPeer(true);
		$options->setSslVerifyStatus(true);
		$cafilepath = $this->_provider->getCertificateFilePath();
		$options->setCaInfo($cafilepath);
		$options->setCaPath($cafilepath);
		
		// }}}
		
		$protocolVersion = 'HTTP_'.\str_replace('.', '_', $request->getProtocolVersion());

		foreach(CurlHttpVersion::cases() as $version)
		{
			if($version->name === $protocolVersion)
			{
				$options->setHttpVersion($version);
				$options->setCustomRequest($request->getMethod());
				break;
			}
		}
		
		$headers = [];
		
		foreach($request->getHeaders() as $key => $values)
		{
			$headers[$key] = \implode("\r\n", $values);
		}
		
		$proxyHeaders = [];

		// filter out specific headers
		foreach($headers as $key => $value)
		{
			$lkey = (string) \mb_strtolower((string) $key);
			if(0 === \mb_strpos($lkey, 'proxy-'))
			{
				$proxyHeaders[$key] = $value;
				unset($headers[$key]);
			}
			if('user-agent' === $lkey)
			{
				// TODO parse user agent
				// $options->setUserAgent($value);
				unset($headers[$key]);
			}
			if('referer' === $lkey)
			{
				$options->setReferer($value);
				unset($headers[$key]);
			}
		}
		
		$headerVals = [];
		
		foreach($headers as $key => $value)
		{
			$headerVals[] = ((string) $key).': '.$value;
		}
		
		$options->setHttpHeaders($headerVals);
		$options->setProxyHeaders($proxyHeaders);
		
		// TODO when stream is a file
		$options->setPostFields($request->getBody()->__toString());
		$schemeMaj = \strtoupper($request->getUri()->getScheme());

		foreach(CurlProtocol::cases() as $value)
		{
			if($value->name === $schemeMaj)
			{
				$options->setProtocol($value);
				break;
			}
		}
		
		return $options;
	}
	
	/**
	 * Creates a new HttpCurlOptions object from the context array on the 
	 * native php stream format.
	 * 
	 * @param array<string, array<string, null|boolean|integer|float|string>> $context
	 * @return CurlOptions
	 * @throws RuntimeException if the argument types are wrong
	 * @see https://secure.php.net/manual/fr/function.stream-context-create.php
	 */
	public function createFromContextArray(array $context) : CurlOptions
	{
		$options = new CurlOptions();
		
		foreach($context as $key => $value)
		{
			switch(\mb_strtolower($key))
			{
				case 'http':
				case 'https':
					foreach($value as $option => $content)
					{
						switch(\mb_strtolower($option))
						{
							case 'method':
								$options->setCustomRequest((string) $content);
								break;
							
							case 'header':
								$options->setHttpHeaders(\explode("\r\n", (string) $content));
								break;
							
							case 'user_agent':
// 								$options->setUserAgent($content);
								break;
							
							case 'content':
								$options->setPostFields((string) $content);
								break;
							
							case 'proxy':
								$options->setProxy((string) $content);
								break;
							
							case 'request_fulluri':
								// TODO
								break;
							
							case 'follow_location':
								$options->setFollowLocation((bool) $content);
								break;
							
							case 'max_redirects':
								$options->setMaxRedirs((int) $content);
								break;
							
							case 'protocol_version':
								try
								{
									$version = CurlHttpVersion::from((int) (string) $content);
								}
								catch(TypeError|ValueError $exc)
								{
									$version = null;
								}
								if(null === $version)
								{
									throw new RuntimeException('Failed to find an http version with number '.((string) $content));
								}
								$options->setHttpVersion($version);
								break;
							
							case 'timeout':
								$options->setTimeout((int) $content);
								break;
							
							case 'ignore_errors':
								// TODO
								break;
						}
					}
					break;
				
				case 'ftp':
				case 'ftps':
					foreach($value as $option => $content)
					{
						switch(\mb_strtolower($option))
						{
							case 'overwrite':
								$options->setFtpAppend(!(bool) $content);
								break;
							
							case 'resume_pos':
								$options->setResumeFrom((int) $content);
								break;
							
							case 'proxy':
								$options->setProxy((string) $content);
								break;
						}
					}
					break;
				
				case 'tls':
				case 'ssl':
					foreach($value as $option => $content)
					{
						switch(\mb_strtolower($option))
						{
							case 'peer_name':
								// TODO
								break;
							
							case 'verify_peer':
								$options->setSslVerifyPeer((bool) $content);
								break;
							
							case 'verify_peer_name':
// 								$options->setSslVerifyHost($content);
								break;
							
							case 'allow_self_signed':
								// TODO
								break;
							
							case 'cafile':
								$options->setCaInfo((string) $content);
								break;
							
							case 'capath':
								$options->setCaPath((string) $content);
								break;
							
							case 'local_cert':
								$options->setSslCert((string) $content);
								break;
							
							case 'local_pk':
								$options->setSslKey((string) $content);
								break;
							
							case 'passphrase':
								$options->setSslKeyPassword((string) $content);
								break;
							
							case 'cn_match':
								// TODO
								break;
							
							case 'verify_depths':
								// TODO
								break;
							
							case 'ciphers':
								$options->setSslCipherList((string) $content);
								break;
							
							case 'capture_peer_cert':
								// TODO
								break;
							
							case 'capture_peer_cert_chain':
								// TODO
								break;
							
							case 'sni_enabled':
								// TODO
								break;
							
							case 'sni_server_name':
								// TODO
								break;
							
							case 'disable_compression':
								// TODO
								break;
							
							case 'peer_fingerprint':
								$options->setSshHostPublicKeyMd5((string) $content);
								break;
						}
					}
					break;
				
				case 'curl':
					foreach($value as $option => $content)
					{
						switch(\mb_strtolower($option))
						{
							case 'method':
								$options->setCustomRequest((string) $content);
								break;
							
							case 'header':
								$options->setHttpHeaders(\explode("\r\n", (string) $content));
								break;
							
							case 'user_agent':
// 								$options->setUserAgent($content);
								break;
							
							case 'content':
								$options->setPostFields((string) $content);
								break;
							
							case 'proxy':
								$options->setProxy((string) $content);
								break;
							
							case 'max_redirects':
								$options->setMaxRedirs((int) $content);
								break;
							
							case 'curl_verify_ssl_host':
// 								$options->setSslVerifyHost($content);
								break;
							
							case 'curl_verify_ssl_peer':
								$options->setSslVerifyPeer((bool) $content);
								break;
						}
					}
					// break
					// case default
			}
		}
		
		return $options;
	}
	
}
