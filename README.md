# php-extended/php-http-client-curl
A psr-18 compliant client with the curl engine.

![coverage](https://gitlab.com/php-extended/php-http-client-curl/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-curl/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-curl ^8`


## Basic Usage

This library is a client using the curl library compiled with php. This library
may be used the following way :

```php

use PhpExtended\HttpClient\CurlClient;

$curlProcessor = new CurlClient();
$response = $curlProcessor->sendRequest($request);
$response->getBody()->__toString();

```


## License

MIT (See [license file](LICENSE)).
